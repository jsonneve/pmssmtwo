"""
.. module:: interfacing.__init__
   :synopsis: This package provides an interface to sqlite3
              in order to store data safely with e.g. checking
              names of columns.

"""

import os

basepath = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..'))
