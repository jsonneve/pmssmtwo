#!/usr/bin/env python
import subprocess
import functools
import math
from scipy.stats import poisson as scipypoisson


def rough_limit(ae, lum = 4.98 * 1000.0, bg_err = 9.4):
        '''(float, float, float) -> float
        Calculate roughly the limit on the cross section in
        pb based on the luminosity in pb, acceptance * efficiency
        (0-1), and the background error. In this case the rough limit
        is twice the background uncertainty divided by the luminosity
        and acceptance*efficiency, as done in arXiv:1102.5338.
        Using the background of CMS of arXiv:1207.1898 in bin
        HT > 1400 GeV and MHT > 200 GeV we have a total background of
        19.0 +- 9.4 for a number of 10000 events.
        '''
        #bg_err = 9.4
        #lum = 4.98 * 1000 #pb
        if ae != 'no data' and not ae == 0 and not ae == None and not ae == '':
                return 2.0*bg_err/(lum*ae)
        else:
                return None



def roostats_limit( bg = 19, bg_err = 9.4, data = 16, NToys = 2000, lumi = 12.9 * 1000.0, lumi_err = 0.11 * 1000.0, ae=0.1, ae_err_pct = 0.2):
    '''(float, [float, float, float, int, float, int, int]) -> float, float
    Calculate 95% CL upper limit on cross section
    using roostats_CL95. Return the upper limit and
    the error on it.
    This relies on the programs my_limit.C and roostats_cl95.C .
    Note that sometimes changes need to be made to roostats_cl95 depending on the version of ROOT!

    >>> roostats_limit(ae=0.2, lumi=12.9*1000.0, lumi_err=0.11*1000.0, ae_err_pct=0.2, bg=5300.49, bg_err=73.49,  data=5069)
    (0.0361499, 0.00725739)

    '''
    ae_err = ae_err_pct * ae
    my_limit = 'my_limit.C("cls",' + str(lumi) + ',' + str(lumi_err) + ',' + str(ae) + ',' + str(ae_err) + ',' + str(bg) + ',' + str(bg_err) + ',' + str(data) + ',' + str(NToys) + ')'
    out = subprocess.check_output(['root.exe', '-b', '-q', my_limit])
    sigma = float(out.split()[-3])
    sigma_err = float(out.split()[-1])
    return sigma, sigma_err


def cls_95(function, lower=0, upper=14000, tolerance=0.000001, pvalue=0.05):
    '''(functools.partial, float, float, float) -> float
    Find the value middle that will satisfy the function to
    evaluate to the pvalue through bisecting the parameter
    space between the lower and upper values. Return the
    middle value when a value within a certain error
    (tolerance) is reached.

    >>> sum([6159, 2305, 454, 62, 808, 305, 124, 52, 335, 129, 34, 32, 98, 38, 23, 94, 39])
    11091
    >>> sum([6232, 2904, 1965, 552, 177, 58, 16, 25])
    11929

    Since these are the sums of the data of the MHT 8 TeV and
    alphaT 8TeV analyses, respectively, these will be taken as
    a guide for the upper value of signal events.

    >>> def myfunc0(x): return x**3 + x - 1
    >>> round(cls_95(myfunc0, lower=0, upper=1, tolerance=5e-7, pvalue=0), 4)
    0.6823
    >>> def myfunc(x, pvalue): return x**3 + x - 1 #+ pvalue
    >>> f = functools.partial(myfunc, pvalue=0.05)
    >>> round(f(0.682328224182))
    0.0
    >>> round(cls_95(f, lower=0, upper=1, tolerance=5e-7, pvalue=0), 4)
    0.6823
    >>> round(cls_95(myfunc0, lower=0, upper=1, tolerance=5e-7, pvalue=0), 4)
    0.6823
    '''
    middle = (lower + upper)/2.0
    while (upper - lower)/2.0 > tolerance:
        if function(middle) == pvalue:
            return middle
        elif (function(lower) - pvalue >= 0 and function(middle) < pvalue) or (function(lower) - pvalue < 0 and function(lower)*function(middle) - pvalue < 0):
        #elif function(lower)*function(middle) < pvalue:
            upper = middle
        else :
            lower = middle
        middle = (lower + upper)/2.0

    return middle

def poisson_pvalue(s, b, d):
    '''(float, float, int) -> float
    Calculate the poisson expected pvalue according to
    the CLs technique for one channel only (no combinations).
    >>> round(poisson_pvalue(math.log(1/0.05), 1, 0), 2)
    0.05
    >>> val = round(math.exp(-1) * 1.5, 2)
    >>> valp = round(poisson_pvalue(1, 1, 1), 2)
    >>> val == valp
    True
    '''
    sum_sb = scipypoisson.cdf(d, s+b)
    sum_b = scipypoisson.cdf(d, b)
    #sum_sb = sum_b = 0
    #for i in range(d + 1):
    #    sum_sb += poisson(s + b, i)
    #    sum_b += poisson(b, i)
    return sum_sb/float(sum_b)


def poisson(l, n):
    '''(float, int) -> float
    Calculate the Poisson distribution value for
    the mean l and at point n.
    For example:
    For signal s, background b, and data d you take the
    sum from 0 to d of all these poisson values with
    mean s+b at point i of this range.
    >>> sumpoisson = sum([poisson(10, i) for i in range(12)])
    >>> sumscipypoisson = scipypoisson.cdf(11, 10)
    >>> round(sumpoisson,4) == round(sumscipypoisson,4)
    True
    '''
    return math.exp(-l) * l**n / float(math.factorial(n))

def find_s_exp(b, d, lower=0, upper=14000, tolerance=0.0000001, pvalue=0.05, distribution=poisson_pvalue):
    '''(float, int, float) -> float
    Find the upper limit on the number
    of events from experiment when demanding
    a pvalue as given.
    >>> find_s_exp(10, 11)
    8.989857822598424
    >>> round(poisson_pvalue(8.989857822598424, 10, 11), 2)
    0.05
    >>> round(poisson_pvalue(find_s_exp(10, 11), 10, 11), 2)
    0.05
    '''
    f = functools.partial(distribution, b=b, d=d)
    return cls_95(f, lower=lower, upper=upper, tolerance=tolerance, pvalue=pvalue)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
