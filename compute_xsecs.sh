#!/bin/bash

####! /bin/env zsh unknown


####! /bin/bash


printf "Start time: "; /bin/date 
printf "Job is running on node: "; /bin/hostname 
printf "Job running as user: "; /usr/bin/id 
printf "Job is running in directory: "; /bin/pwd 

# Testing and verbosity
testing=''
verbose=verbose
# uncomment this line below for testing:
# testing=testing


# Set directories
export slhadir=/store/user/jsonneve/slha_mcmc1/pMSSM12_MCMC1_120mh130_56p2K/
export fermioutdir=/store/user/jsonneve/samsbenchmarks/
export workdir=$PWD

# Gather command line argument
filename=${1}

# Set filenames
# In zshell:
#export slha=$filename[29,-1]
#outputname=$slha[0,-6]
# In bash:
export slha=$(basename $filename) # get rid of path up to /
export ext="${slha##*.}"
export outputname="${slha%.*}" # now get rid of extension, too

out1=own_output_$outputname
out2=own_output_nice_$outputname
output=prospino_output_$outputname
#output=$slha[0,-6]$append_out
echo "writing to:" $output
echo "working on:" $filename

# cms software setup
# Setup CMSSW Base
export VO_CMS_SW_DIR=/cvmfs/cms.cern.ch
source $VO_CMS_SW_DIR/cmsset_default.sh
export SCRAM_ARCH=slc6_amd64_gcc530

# Checkout recent cmssw version
if [ -r CMSSW_8_0_20/src ] ; then 
 echo release CMSSW_8_0_20 already exists
else
 scram p CMSSW CMSSW_8_0_20
fi
cd CMSSW_8_0_20/src
eval `scram runtime -sh`

# Get prospino and resummino
wget 'http://stash.osgconnect.net/~jsonneve/prospino_resummino.tar'
tar xf prospino_resummino.tar

# Get the PDFs that you want:
wget 'http://www.hepforge.org/archive/lhapdf/pdfsets/current/CT10nlo_as_0114.LHgrid'
wget 'http://www.hepforge.org/archive/lhapdf/pdfsets/current/cteq61.LHgrid'
export LHAPATH=$PWD

# Get interface code
git clone https://gitlab.cern.ch/jsonneve/pmssmtwo.git
cd pmssmtwo
export PYTHONPATH=$PYTHONPATH:$PWD
cd interfacing

# Copy slha file to local place
echo copying to local dir: $slha
xrdcp root://cmsxrootd.fnal.gov/${slhadir}${filename} $slha
# echo copied to file $slha
ls $slha

# Print some info
echo 'this is your directory:' $PWD
echo 'this is what is in there:' $(ls)
echo 'this is your slha file:', $slha
echo python prospino_interface.py $slha $output ${out1} ${out2} $testing $verbose
echo
echo "Working hard..."

# Run prospino and if necessary resummino
python prospino_interface.py $slha $output $out1 $out2 $testing $verbose

# Copy files to outside cmssw dir just in case
cp $out1 ${workdir}/$testing$out1
cp $out2 ${workdir}/$testing$out2
cp $output ${workdir}/$testing$output
cp $slha ${workdir}/$testing$slha

# Copy output files to fermilab storage:
xrdcp $out1 root://cmseos.fnal.gov/${fermioutdir}$testing${out1}
xrdcp $out2 root://cmseos.fnal.gov/${fermioutdir}$testing${out2}
xrdcp $output root://cmseos.fnal.gov/${fermioutdir}$testing${output}
#gfal-copy "file://$PWD/$out1" "srm://dcache-se-cms.desy.de:8443//srm/managerv2?SFN=/pnfs/desy.de/cms/tier2/store/user/jsonneve/slha/$testing$out1" 2>&1
#gfal-copy "file://$PWD/$out2" "srm://dcache-se-cms.desy.de:8443//srm/managerv2?SFN=/pnfs/desy.de/cms/tier2/store/user/jsonneve/slha/$testing$out2" 2>&1
#gfal-copy "file://$PWD/$output" "srm://dcache-se-cms.desy.de:8443//srm/managerv2?SFN=/pnfs/desy.de/cms/tier2/store/user/jsonneve/slha/$testing$output" 2>&1
cat $output

# Remove everything
cd $workdir
rm -rf CMSSW_8_0_20




echo "Science complete!"
