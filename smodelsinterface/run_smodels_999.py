#!/usr/bin/env python

from __future__ import print_function

"""
.. module:: Example
   :synopsis: Basic main file example for using SModelS.

   This file must be run under the installation folder.

"""

""" Import basic functions (this file must be executed in the installation folder) """

import os
import sys
smodelsdir = '/home/jory/rwth/smodels_official/'
sys.path.append(smodelsdir)
from smodels.theory import slhaDecomposer
from smodels.theory import lheDecomposer
from smodels.tools.physicsUnits import pb, fb, GeV
from smodels.tools.printer import printout
from smodels.theory.theoryPrediction import theoryPredictionsFor
from smodels.experiment.databaseObjects import Database
import subprocess
from math import floor, log10

#Set the address of the database folder
database = Database("/home/jory/rwth/smodels_official/smodels-database-dev/")

def round_to_sign(x, sig=3):
    """
    Round the given number to the significant number of digits.
    """
    return round(x, sig-int(floor(log10(x)))-1)

def run_smodels(slhafile, verbose=False):
    """
    Run SModelS on given slha file.
    Return results.

    """

    # Set correct paths, names
    smodelsdir = '/home/jory/rwth/smodels_official/'
    sys.path.append(smodelsdir)
    dbpath = os.path.join(smodelsdir, "smodels-database-dev/")
    database = Database(dbpath)

    # Write cross sections to file with option -p
    # at NLO + NLL with option -N
    subprocess.check_output(['python2', smodelsdir + 'runTools.py', 'xseccomputer', '-N', '-p', '-f', slhafile])

    #Set main options for decomposition:
    sigmacut = 0.003 * fb
    mingap = 5. * GeV

    """ Decompose model (use slhaDecomposer for SLHA input or lheDecomposer for LHE input) """
    smstoplist = slhaDecomposer.decompose(slhafile, sigmacut, doCompress=True, doInvisible=True, minmassgap=mingap)

    # Print decomposition summary. Set outputLevel=0 (no output), 1 (simple output), 2 (extended output)
    if verbose:
        printout(smstoplist,outputLevel=2)

    # Topologies
    #for topo in smstoplist:
    #    output = "Topology:\n"
    #    output += "Number of vertices: " + str(topo.vertnumb) + ' \n'
    #    output += "Number of vertex parts: " + str(topo.vertparts) + '\n'
    #    totxsec = topo.getTotalWeight()
    #    output += "Total Global topology weight :\n" + totxsec.niceStr() + '\n'
    #    output += "Total Number of Elements: " + str(len(topo.elementList)) + '\n'
    #    if objOutputLevel == 2:
    #            for el in topo.elementList:
    #                    output += "\t\t Element: \n"
    #                    output += self._formatElement(el,1) + "\n"


    # Load all analyses from database
    #listOfExpRes = database.getExpResults(dataTypes=['efficiencyMap'])
    listOfExpRes = database.getExpResults( dataTypes=['upperLimit'], analysisIDs=['ATLAS-CONF-2013-999', 'ATLAS-CONF-2013-037'], txnames=['T2tt','TGQtttt'])
    #listOfExpRes = database.getExpResults()

    # Make a list of results:
    results = []

    # Compute the theory predictions for each analysis
    for expResult in listOfExpRes:
        analysisresult = None
        predictions = theoryPredictionsFor(expResult, smstoplist)
        if not predictions: continue
        an = expResult.getValuesFor('id')[0]
        if verbose:
            print('\n', an)
        for theoryPrediction in predictions:
            dataset = theoryPrediction.dataset
            datasetID = dataset.getValuesFor('dataId')[0]
            mass = theoryPrediction.mass
            txnames = [str(txname) for txname in theoryPrediction.txnames]

            # Stop if it is an efficiency result
            # Dataset should be None and txnames should be of length 1:
            # If not, we're dealing with efficiencies
            if len(txnames) > 1: continue

            PIDs =  theoryPrediction.PIDs
            if verbose:
                print("------------------------")
                print("Dataset = ",datasetID)   #Analysis name
                print("TxNames = ",txnames)
                print("Prediction Mass = ",mass)    #Value for average cluster mass (average mass of the elements in cluster)
                print("Prediction PIDs = ",PIDs)    #Value for average cluster mass (average mass of the elements in cluster)
                print("Theory Prediction = ",theoryPrediction.value)   #Value for the cluster signal cross-section
                print("Condition Violation = ",theoryPrediction.conditions)  #Condition violation values


            #Get upper limit for the respective prediction:
            if expResult.getValuesFor('dataType')[0] == 'upperLimit':
                ul = expResult.getUpperLimitFor(txname=theoryPrediction.txnames[0],mass=mass)
            elif expResult.getValuesFor('dataType')[0] == 'efficiencyMap':
                ul = expResult.getUpperLimitFor(dataID=datasetID)
            else: print('weird:',expResult.getValuesFor('dataType'))
            r = theoryPrediction.value[0].value/ul
            r = round_to_sign(r.asNumber())
            if verbose:
                print("Theory Prediction UL = ",ul)
                print("R = ", r)
            theo = round_to_sign(theoryPrediction.value[0].value.asNumber(pb))
            ulobs = round_to_sign(ul.asNumber(pb))

            # Make a dictionary of the result:
            analysisresult = {'an': an}
            analysisresult['mass'] = str(mass)
            analysisresult['top'] = txnames[0]
            analysisresult['pid'] = str(PIDs)
            analysisresult['theo'] = theo
            analysisresult['ul'] = ulobs
            analysisresult['r'] = r
            if verbose:
                print(analysisresult)


        # append every result for the analysis
        results.append(analysisresult)
    return results







if __name__ == '__main__':
    # If called just like this, run on given slha.
    res = run_smodels(sys.argv[-1], verbose=True)
    print(res)
