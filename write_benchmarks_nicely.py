from storingdata import sql3 as sql
import glob
import os
import numpy as np


resultdir = '/eos/uscms/store/user/jsonneve/samsbenchmarks/'
allfiles = glob.glob(resultdir + 'own_output_pMSSM*')
#'own_output_pMSSM12_MCMC1_30_731068'
#for fi in [testfile]:
#for fi in allfiles[:1]:
#failed_files = [s for s in skip]
failed_files = []
succeeded_files = []
for fi in allfiles:
    print fi
    if sql.point_exists('xsecs13.db', 'samsbenchmarks', {'id': '"' + os.path.basename(fi).replace('own_output_', '').replace('ownnew_output_', '') + '"'}):
        print('file in db', fi)
        succeeded_files.append(os.path.basename(fi))
        continue
    if not os.path.exists(fi):
        print("File does not exist:", fi)
        failed_files.append(fi)
        continue
    #if fi in skip:
    #    print("skipping...", fi)
    #    continue
    f = open(fi, 'r')
    lines = f.read()
    f.close()
    lijstje = [i for i in lines.split() if i.startswith('pMSSM')]
    print('no pmssm in file')
    if len(lijstje) == 0:
        failed_files.append(fi)
        # print("empty file:", fi)
        continue
        #if sql.point_exists('xsecs13.db', 'samsbenchmarks', {'id': os.path.basename(fi).replace('own_output_', '')}):
        #    continue
        #else:
        #    sql.dict_to_db(d, '
    li = lijstje[-1]
    headers = 'id|ng|bb|gg|ll|nn|ns|sb|sg|ss|tb|tot|tot_rel_err|ng_rel_err|bb_rel_err|gg_rel_err|id_rel_err|ll_rel_err|nn_rel_err|ns_rel_err|sb_rel_err|sg_rel_err|ss_rel_err|tb_rel_err|time|'.split('|')
    vals = li.split('|')
    #d = {headers[i]: vals[i] for i in range(len(headers))}
    d = {}
    for i in range(len(headers)):
        d[headers[i]]= vals[i]
    d.pop('')
    if d['tot'] in [None, 'nan', np.nan]:
        print('nan in total', fi)
        failed_files.append(fi)
        continue
    print fi
    print d
    # sql.dict_to_db(d, 'xsecs13.db', 'samsbenchmarks')
    succeeded_files.append(os.path.basename(fi))
really_failed_files = [os.path.basename(fi) for fi in failed_files if os.path.basename(fi) not in succeeded_files]
unique_failed_files = list(set(really_failed_files))
print("succeeded files")
print(succeeded_files)
print("Failed files:")
for fi in unique_failed_files:
    print(fi)

