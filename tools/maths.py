#!/usr/bin/env python3


"""
Tools for mathematical computations.
"""

from math import floor, log10

def round_to_sign(x, sig=3):
    """
    Round the given number to the significant number of digits.
    """
    return round(x, sig-int(floor(log10(x)))-1)



