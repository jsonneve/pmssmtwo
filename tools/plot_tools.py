#!/usr/bin/env python3

""" 
    plot_tools.py
    Tools for plotting.

"""

from matplotlib import pyplot as plt
from functools import partial
import numpy as np

def make_bins_xy(x, y, steps):
    """
    Given a set of points (x, y)
    separately as a list of x 
    and a list of y, make appropriate bins
    of the number steps wide.
    x and y must be lists of equal length.

    >>> make_bins_xy([1, 2, 3, 4], [3, 4, 5, 6], 1)
    [array([ 0.5,  1.5,  2.5,  3.5,  4.5]), array([ 2.5,  3.5,  4.5,  5.5,  6.5])]

    """

    if not len(x) == len(y):
        print('plot_tools.make_bins_xy: input lists are of unequal lengths')
        return None

    return list(map(partial(make_bins, width=steps), [x, y]))


def make_bins(points, width):
    """
    Given a set of points,
    make proper bins out of it
    with the given width.

    >>> make_bins([1, 2, 3, 4], 1)
    array([ 0.5,  1.5,  2.5,  3.5,  4.5])
    """

    # Find lower edge of lower end bin
    min_pt = min(points)- width/2.0
    # Find upper edge of higher end bin
    max_pt = max(points) + width/2.0 + width

    # Return the bin boundaries
    return np.arange(min_pt, max_pt, width)

def make_bins_int(points, width):
    """
    Given a set of points,
    make proper bins out of it
    with the given width.

    >>> make_bins([2, 4, 6, 8], 2)
    array([ 1.,  3.,  5.,  7.,  9.])
    """

    # Find lower edge of lower end bin
    min_pt = int(min(points))- int(width/2.0)
    # Find upper edge of higher end bin
    max_pt = int(max(points)) + int(width/2.0) + width

    # Return the bin boundaries
    return range(min_pt, max_pt, width)



if __name__ == "__main__":
    import doctest
    doctest.testmod()