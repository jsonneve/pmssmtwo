"""
.. module:: tools.__init__
   :synopsis: This package provides tools in hep
              computing.
              Any repetition found on the way that has
              to do with 
                  * os
                  * string manipulation
                  * file manipulations
                  * math computations
                  * ...
              should be added here.

"""

import os

basepath = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..'))
