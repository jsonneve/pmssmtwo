#!/usr/bin/env python3

"""
Tools for os actions
like unzipping files or
reading hostnames etc.
"""



import os
import socket
import subprocess

def home_dir():
    name = socket.gethostname()
    if 'top' in name or 'streep' in name or 'koekblik' in name:
        return '/home/jory/hh/'
    else: 
        return '/afs/desy.de/user/s/sonnevej/'


def unzip_file(zipped_filename):
        """ (str) -> str
        Open a zipped file and unzip it; return the unzipped
        filename.
        Assuming gzipped files with extension .gz
        """
        if zipped_filename[-2:] != 'gz':
            return zipped_filename
        out = subprocess.check_output(['gunzip', zipped_filename])
        unzipped_filename = zipped_filename[:-3]
        return unzipped_filename



