#!/usr/bin/env python3

"""
Tools for manipulating text,
both strings and text in files.
"""


def make_nice_line(qtys, sep='\t', just=10):
    """
    Return a nice printable string so that
    the quantities are in table form.
    """
    return ''.join([str(qty).ljust(just) + sep for qty in qtys])

def make_line(qtys, sep='\t'):
    """
    Return a line with the quantities
    separated by sep.
    """
    length_sep = len(sep)
    return ''.join([str(qty) + sep for qty in qtys])[:-length_sep]

def write_and_replace(template, destination, replacements):
    r"""(str, str, dict) -> str
    Read out template and save to destination with
    the keys of replacements replaced by the corresponding
    values.
    >>> template = 'testing_temp.dat'
    >>> destination = 'testing_dest.dat'
    >>> fopen = open(template, 'w')
    >>> written = fopen.write('In this file\nI will replace\nsome words')
    >>> fopen.close()
    >>> replacements = {'replace': 'swap', 'words': 'letters', 'file': 'text'}
    >>> write_and_replace(template, destination, replacements)
    'testing_dest.dat'
    >>> fopen = open(destination, 'r')
    >>> fopen.read()
    'In this text\nI will swap\nsome letters'
    """
    source = open(template, 'r')
    dest = open(destination, 'w')
    line = source.readline()
    while line != '':
        for key in replacements:
            line = line.replace(key, str(replacements[key]))
        dest.write(line)
        line = source.readline()
    source.close()
    dest.close()
    return destination




