#!/usr/bin/env python3

"""
Interface to MadGraph.

Get MadGraph from LaunchPad:
wget 'https://launchpad.net/mg5amcnlo/2.0/2.4.x/+download/MG5_aMC_v2.4.3.tar.gz'
tar xf MG5_aMC_v2.4.3.tar.gz
cd MG5_aMC_v2_4_3

Change the file
cd madgraph/various/misc.py
to read 
        if extension in ['html','htm','php']:
            pass
            #self.open_program(self.web_browser, filename, background=True)
to prevent it from opening tabs in your browser.


"""

from __future__ import division
import os
import shutil as sh
import subprocess
import math
import locale
import glob


def copy_cards(processdir, slhafile, runcard=None, pythiacard=None):
    """
    Copy files to madgraph directory.
    """
    sh.copyfile(slhafile, os.path.join(processdir, "Cards/param_card.dat"))
    if runcard != None:
        sh.copyfile(runcard, os.path.join(processdir, "Cards/run_card.dat"))
    if pythiacard != None:
        sh.copyfile(pythiacard, os.path.join(processdir, "Cards/pythia_card.dat"))


def mg5rundir(processdir, runname):
    """
    Return the full rundirectory in the Events
    directory.
    """
    return os.path.join(os.path.join(processdir, 'Events'), runname)

def mg5_file(processdir, runname, pythia=True, multicore='--multicore', verbose=False):
    """
    Run madgraph and pythia in processdir with slha parameter card
    and pythia and run cards.
    """
    check_runweb(processdir)
    mg5_rundir = mg5rundir(processdir, runname)
    if os.path.exists(mg5_rundir):
        remove_mg5run(processdir, runname)
        if verbose:
            print("Removed existing madgraph files:", mg5_rundir)
    if pythia:
        laststep = "--laststep=pythia"
        level = 'pythia'
    else:
        laststep = "--laststep=parton"
        level = 'parton'
    out = subprocess.check_output(['python2', os.path.join(processdir, "bin/generate_events"), runname, laststep, multicore, "-f"])
    encoding = locale.getdefaultlocale()[1]
    s = out.decode(encoding)
    if 'Error' in s or 'ERROR' in s:
        print("There was an error while running madgraph.")

    # Retrieve results summary:
        ## Search for output as follows:
        #  === Results Summary for run: run_01 tag: tag_1 ===
        #
        #       Width :   78.85 +- 0.1666 GeV
        #            Nb of events :  10000
    phrase = '=== Results Summary for run:'
    if phrase in s:
        # Search for latest phrase: when matching, there are 2 summaries
        results_summary = s[s.rindex(phrase):]
        results_summary = results_summary[:results_summary.index('INFO: ')]
    else:
        print("No output from generating " + level + " level events. Output was: ", s)
        results_summary = 'no summary'

    ######## Verbosity #######
    if verbose:
        print("Running " + level + " level on ", os.path.join(processdir, '/Events/') + runname)
        #
        print(results_summary)
        print("Ran " + level + ":\n", )
    #########################

    # Even when removing files, the tag names increase building on the previous;
    # Take this into account:
    # But I think only tag_1 exists for hep files; another tag is never created.
    if pythia:
        filename = glob.glob(os.path.join(processdir, 'Events/') + runname + '/tag_*_pythia_events.hep.gz')[-1]
    else:
        filenames = glob.glob(os.path.join(processdir, 'Events/') + runname + '/unweighted*.gz')
        if filenames == []: return None
        filename = filenames[-1]

    return filename, results_summary


def extract_xsec(resultssummary, matched=False):
    """
    Extract the cross section and its error from the
    results summary of MG5.
    Other xsecname: 
    """
    xsecname= 'Cross-section'
    if xsecname not in resultssummary:
        # Another name used by MG5 if it concerns a width:
        xsecname = 'Width'
    neventsname = 'Nb of events'
    if matched:
        xsecname = 'Matched Cross-section'
        neventsname += ' after Matching'
    print(xsecname)
    print(resultssummary)
    xsec = resultssummary[resultssummary.index(xsecname + ' : '):].split()[2]
    xsecerror = resultssummary[resultssummary.index(xsecname + ' : '):].split()[4]
    nevents = resultssummary[resultssummary.index(neventsname + ' :'):].split()[4]
    # another way:
    #res.split('\n')[2].split(':')[1].split()[0]
    return xsec, xsecerror, nevents


def check_runweb(processdir):
        '''None -> None
        Check if a Runweb file exists in Madgraph and
        remove it if it is there so Madgraph can be run.
        Notify if this is done.
        '''
        if os.path.exists(processdir + '/RunWeb'):
                os.remove(processdir + '/RunWeb')
                print("Removed a RunWeb file")


def remove_mg5run(processdir, runname):
    """
    Remove the entire run directory.
    Note that in subprocesses large files remain.
    For this reason, frequently regenerate the process
    directory.
    """
    rundir = mg5rundir(processdir, runname)
    if os.path.exists(rundir):
        sh.rmtree(rundir)
    else:
        print("Run directory " + rundir + " does not exist!")



def generate_mg5_dir(mg5dir, proccard):
    '''
    Generate directory in madgraph using this process card.
    '''
    curdir = os.getcwd()
    os.chdir(mg5dir)
    out = subprocess.check_output(['python2', 'bin/mg5_aMC', os.path.join(curdir, proccard)])
    os.chdir(curdir)

    # To obtain dirname:
    # Fetch this line:
    # Output to directory /home/jory/bin/MadGraph5_v1_5_12/sq_goq done
    encoding = locale.getdefaultlocale()[1]
    s = out.decode(encoding)
    outputdir = s[s.index('Output to directory '):].split()[3]
    proccardname = proccard[:proccard.index('_proc_card_mg5.dat')]
    if outputdir.split('/')[-1] != proccardname.split('/')[-1]:
        print('Different output directory from MG5 than what you named the proc card:')
        print('Outputdir MG5:' + outputdir)
        print('You named the proccard:' + proccardname)
    return outputdir

if __name__ == '__main__':
    import doctest
    doctest.testmod()
    slha = os.path.abspath('pMSSM12_MCMC1_13_655251.slha')
    mg5dir = '/afs/desy.de/user/s/sonnevej/xxl/MG5_aMC_v2_4_3'
    runcard = 'run_13TeV_10000_evts.dat'
    for proc in 'sqsq', 'gogo':
        proc_card = 'pp_' + proc + '_proc_card_mg5.dat'
        proc_dir = generate_mg5_dir(mg5dir, proc_card)
        print("process dir:", proc_dir)
        #proc_dir = "/afs/desy.de/group/cms/pool/sonnevej/MG5_aMC_v2_4_3/pp_sqsq"
        copy_cards(proc_dir, slha, runcard=None, pythiacard=None)
        runname = proc
        outfile, results = mg5_file(proc_dir, runname, pythia=False, multicore='--multicore',
                verbose=False)
        print("outfile:", outfile)
        print("output:", results)

        xsec, xsecerr, nevts =  extract_xsec(results, matched=False)
        print("xsec", xsec, '+-', xsecerr, 'for', nevts, 'events')
        remove_mg5run(proc_dir, runname)
