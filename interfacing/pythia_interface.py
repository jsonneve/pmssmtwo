#!/usr/bin/env python3

"""
Interface to MadGraph.
"""

from __future__ import division
import os
import shutil as sh
import subprocess
import math
import locale
import glob


def run_pythia(pythiadir, lhefile, maincc, maincmnd):
    """
    Run pythia8 on the given lhefile
    with the given parameters in maincmnd
    and the given main program maincc.
    """

    # Return information from pythia
    # Look for information like this:
    #  
    # *-------  PYTHIA Event and Cross Section Statistics  -------------------------------------------------------------*
    # |                                                                                                                 |
    # | Subprocess                                    Code |            Number of events       |      sigma +- delta    |
    # |                                                    |       Tried   Selected   Accepted |     (estimated) (mb)   |
    # |                                                    |                                   |                        |
    # |-----------------------------------------------------------------------------------------------------------------|
    # |                                                    |                                   |                        |
    # | Les Houches User Process(es)                  9999 |       10000      10000       3895 |   5.811e-12  7.348e-14 |
    # |    ... whereof user classification code          1 |        3441       3441       1049 |                        | 
    # |    ... whereof user classification code          2 |        6559       6559       2846 |                        | 
    # |                                                    |                                   |                        |
    # | sum                                                |       10000      10000       3895 |   5.811e-12  7.348e-14 |
    # |                                                                                                                 |
    # *-------  End PYTHIA Event and Cross Section Statistics ----------------------------------------------------------*

    curdir = os.getcwd()
    sh.copy(maincc, os.path.join(pythiadir, 'examples/main00.cc'))
    sh.copy(maincmnd, os.path.join(pythiadir, 'examples/main00.cmnd'))
    os.chdir(pythiadir)
    outmake = subprocess.check_output(['gmake', 'main00'])
    out = subprocess.check_output(['./main00'])
    encoding = locale.getdefaultlocale()[1]
    s = out.decode(encoding)
    phrase = 'PYTHIA Event and Cross Section Statistics'
    if phrase in s:
        xsecerror = float(s.split('\n')[11].split()[-2]) * 10*9 # pb (given in mb)
        xsec = s.split('\n')[11].split()[-3] * 10*9 # pb (given in mb)
    hepmc = os.path.join(pythiadir, 'examples/main00.hepmc')
    return hepmc, xsec, xsecerror

