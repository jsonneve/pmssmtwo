#!/usr/bin/env python3

import subprocess
import shutil as sh
import os


def smodels_xsec_to_slha(slha, smodelsdir, order="NLL", sqrts=13,
        write_to_file=True, verbose = False):
    """
    Run SModelS xseccomputer on slha file
    to write cross sections to the file.
    """

    curdir = os.getcwd()
    os.chdir(smodelsdir)
    #args = ["python2", "smodelsTools.py", "xseccomputer"]
    args = ["python2", "runTools.py", "xseccomputer"]
    args += ["--filename", slha]
    args += ["--"+ order]
    args += ["--sqrts", str(sqrts)]
    args += ["--tofile"]
    if verbose:
        print(''.join([arg + ' ' for arg in args]))
    out = subprocess.check_output(args)
    os.chdir(curdir)
    return out

def sum_xsecs(smodels_output):
    """
    Put output cross sections into dictionary.

    Example:

    >>> out =  "     Cross sections:\n"
    >>> out += " =======================\n"
    >>> out += "13 TeV (NLO)  (-2000001, 1000002):  1.996e-05 pb\n"
    >>> out += "13 TeV (NLO)  (-1000002, 2000002):  5.364e-05 pb\n"
    >>> out += "13 TeV (NLO)  (1000021, 2000002):  2.146e-04 pb\n"
    >>> 1.996e-05 + 5.364e-05
    7.36e-05
    >>> sum_xsecs(out)
    {'sb': 7.36e-05, 'sg': 2.146e-04, 'tot': 0.0002882}
    
    
    Also possible:
    {'13': {'NLO': {'sb': 7.36e-05, 'sg': 2.146e-04, 'tot': 0.0002882}}}
    Assuming here that sqrts is set as well as order (only one given).
    """

    xsecs = {}
    keys = ['tot', 'sg', 'sb' 'nn', 'ng', 'ss', 'gg', 'll', 'ns']
    lines = str(out).split('\n')
    for line in lines:
        if 'TeV' in line:
            sqrts = line.split()[0]
            order = line.split()[2].strip(')').strip('(')
            if sqrts=given_sqrts and order = given order:
                pass
                # smodels is doing all this





if __name__ == "__main__":
    smodelsdir = '/afs/desy.de/user/s/sonnevej/smodels/'
    #smodelsdir = '/afs/desy.de/user/s/sonnevej/xxl/testsmodels/smodels-v1.0.4'
    #args = "python2 smodelsTools.py xseccomputer --filename pMSSM12_MCMC1_33_515635_withxsecs.slha --NLO --sqrts 13 --tofile"
    slha = os.path.abspath("pMSSM12_MCMC1_13_655251_withxsecs.slha")
    print("Running smodels on", slha)
    out = smodels_xsec_to_slha(slha, smodelsdir, verbose=True)
    print(out)