#!/usr/bin/env python3

"""
Interface to CheckMATE.
"""

from __future__ import division
import os
import shutil as sh
import subprocess
import math
import numpy as np
import locale
import glob
# Issues with python2:
try:
    import configparser
except ImportError as e:
    print('There was an error importing configparser; will import ConfigParser instead (python2)')
    import ConfigParser as configparser
import csv


def upper_limits(checkmate_resultdir, paramfile, verbose=False):
    """(string, string, bool) -> dict

    Return best signal region result for each
    analysis for given result directory.

    """
    # Retrieve r-values for best signal regions (for each analysis)
    best_srs = os.path.join(checkmate_resultdir, 'evaluation/best_signal_regions.txt')
    xsec = get_xsec_pb(paramfile)
    reader = csv.DictReader(open(best_srs, 'r'), delimiter=' ', skipinitialspace=True)
    dic = [d for d in reader]

    # Add upper limits (observed and expected) to result dictionary:
    uls = list(map(add_uls, dic, list(np.ones(len(dic))*xsec)))

    # Return this dictionary and the result directory.
    return uls

def run_checkmate(checkmatedir, paramfile, verbose=False):
    """(string, string, bool) -> str
    
    Run CheckMATE (located in checkmatedir)
    on given paramfile.
    Return the result directory.
    """
    # Obtain name of directory with results
    runname = get_run_name(paramfile)
    outputdir = os.path.join(os.path.join(checkmatedir, 'results'), runname)

    # Remove this directory if it already exists
    check_outputdir_exists(outputdir)

    # Run CheckMATE
    out = subprocess.check_output(['python2', os.path.join(checkmatedir, 'bin/CheckMATE'), paramfile])
    # Print output if wished
    if verbose:
        encoding = locale.getdefaultlocale()[1]
        s = out.decode(encoding)
        print(s)
    return outputdir



def add_uls(d, xsec):
    """(dict, float) -> dict
    Given a dictionary with rvalues,
    return the same dictionary with 
    xsec and obs and exp upper limits.
    """
    ulobs = xsec/float(d['r_obs^c'])
    ulexp = xsec/float(d['r_exp^c'])
    d.update({'xsec': xsec, 'ulobs': ulobs, 'ulexp': ulexp})
    return d


def check_outputdir_exists(outputdir):
    """(str) -> None
    Check if CheckMATE resultdir exists.
    If so, remove.
    """
    if os.path.exists(outputdir):
        sh.rmtree(outputdir)

def get_run_name(paramfile):
    """(str) -> str
    Extract runname.
    """
    config = configparser.ConfigParser()
    config.read(paramfile)
    return config.get('Mandatory Parameters', 'Name')

def get_xsec_pb(paramfile, proc='gluinosquark'):
    """(str, str) -> str
    Extract runname.
    """
    config = configparser.ConfigParser()
    config.read(paramfile)
    float('  0.726E-02*PB'.split('*')[0])
    xsec = config.get(proc, 'XSect')
    xsecpb = float(xsec.split('*')[0])
    if 'FB' in xsec:
        xsecpb *= 1000
    return xsecpb



