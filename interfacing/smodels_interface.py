#!/usr/bin/env python3


from __future__ import print_function

import os
import sys
import socket

sys.path.append('..')
from tools import maths
from tools import os_tools
from interfacing import nllfast_interface as nllf

# Define homedir
name = socket.gethostname()
HOME = os_tools.home_dir()

smodelsdir = os.path.join(HOME, 'smodels/')
bindir = os.path.join(HOME, 'bin')
#pythondir = os.path.join(bindir, 'lib64/python3.4/site-packages')
sys.path.append(smodelsdir)
sys.path.append(bindir)
#sys.path.append(pythondir)
from smodels.theory import slhaDecomposer
from smodels.theory import lheDecomposer
from smodels.tools.physicsUnits import pb, fb, GeV
from smodels.theory.theoryPrediction import theoryPredictionFor
from smodels.experiment import smsHelpers, smsAnalysisFactory
import subprocess

#Set the address of the database folder
databasedir = os.path.join(HOME, "smodels_official/smodels-database-dev/")
print("Will now load SModelS database:", databasedir)

def no_brackets(topname):
    """
    Remove brackets from SModelS topology name.
    Separate branches with a semicolon.
    """
    return str(topname).replace("'", '').replace(']]', ';').replace('[',
                '').replace(']', '')

def run_smodels(slhafile, verbose=False):
    """
    Run SModelS on given slha file.
    Return results.

    """

    # Set correct paths, names
    smodelsdir = os.path.join(HOME, 'smodels_official/')
    sys.path.append(smodelsdir)
    #dbpath = os.path.join(smodelsdir, "smodels-database-dev/")
    smsHelpers.base = parser.get("path", "databasePath")
    #database = Database(dbpath)
    return run_smodels_with_db(slhafile, database, smodelsdir, verbose=verbose)



def write_xsecs(slhafile, smodelsdir, order='-N', sqrts='-s 8', verbose=False):
    """
    Write xsecs to file (in pb) using SModelS
    functions.
    Default is NLO + NLL using NLLFast.
    Other orders: 
        '-n' for NLO
        '' for LO
    Default sqrt(s) is taken from SModelS;
    can also be of the form:
        '-s 13 TeV'

    """
    subprocess.check_output(['python2', os.path.join(smodelsdir, 'runTools.py'), 'xseccomputer', order, sqrts, '-p', '-f', slhafile])


def run_smodels_with_db(slhafile, database, smodelsdir, order='-N', sqrts='', analyses='all', txnames='all', verbose=False):
    """
    Run SModelS located in smodelsdir on given slha file
    using given readily loaded database.
    Return results.
    """

    # Write cross sections to file with option -p
    # at NLO + NLL with option -N
    write_xsecs(slhafile, smodelsdir, order, sqrts, verbose)

    #Set main options for decomposition:
    sigmacut = 0.003 * fb
    mingap = 5. * GeV

    """ Decompose model (use slhaDecomposer for SLHA input or lheDecomposer for LHE input) """
    smstoplist = slhaDecomposer.decompose(slhafile, sigmacut, doCompress=True, doInvisible=True, minmassgap=mingap)

    # Print decomposition summary. Set outputLevel=0 (no output), 1 (simple output), 2 (extended output)
    if verbose:
        smstoplist.printout(outputLevel=2)

    # Load all analyses from database
    listofanalyses = smsAnalysisFactory.load(analyses, txnames)

    return smodels_results(smstoplist, listofanalyses, verbose=verbose)

def smodels_results(smstoplist, listofanalyses, verbose=False):
    """
    Run SModelS for all given experimental results for the given
    sms topology list.
    Return a list of dictionaries with the results for each analysis,
    each topology, each mass.
    """

    # Make a list of results:
    results = []

    # Compute the theory predictions for each analysis
    for analysis in listofanalyses:
        analysisresult = None
        predictions = theoryPredictionFor(analysis, smstoplist)
        if not predictions: continue
        an = analysis.getValuesFor('id')[0]
        if verbose:
            print('\n', an)
        for theoryPrediction in predictions:
            dataset = theoryPrediction.dataset
            datasetID = dataset.getValuesFor('dataId')[0]
            mass = theoryPrediction.mass
            txnames = [str(txname) for txname in theoryPrediction.txnames]

            # Stop if it is an efficiency result
            # Dataset should be None and txnames should be of length 1:
            # If not, we're dealing with efficiencies
            if len(txnames) > 1: continue

            PIDs =  theoryPrediction.PIDs
            if verbose:
                print("------------------------")
                print("Dataset = ",datasetID)   #Analysis name
                print("TxNames = ",txnames)
                print("Prediction Mass = ",mass)    #Value for average cluster mass (average mass of the elements in cluster)
                print("Prediction PIDs = ",PIDs)    #Value for average cluster mass (average mass of the elements in cluster)
                print("Theory Prediction = ",theoryPrediction.value)   #Value for the cluster signal cross-section
                print("Condition Violation = ",theoryPrediction.conditions)  #Condition violation values


            #Get upper limit for the respective prediction:
            if analysis.getValuesFor('dataType')[0] == 'upperLimit':
                ul = analysis.getUpperLimitFor(txname=theoryPrediction.txnames[0],mass=mass)
            elif analysis.getValuesFor('dataType')[0] == 'efficiencyMap':
                ul = analysis.getUpperLimitFor(dataID=datasetID)
            else: print('weird:',analysis.getValuesFor('dataType'))
            r = theoryPrediction.value[0].value/ul
            r = mathss.round_to_sign(r.asNumber())
            if verbose:
                print("Theory Prediction UL = ",ul)
                print("R = ", r)
            theo = maths.round_to_sign(theoryPrediction.value[0].value.asNumber(pb))
            ulobs = maths.round_to_sign(ul.asNumber(pb))

            # Make a dictionary of the result:
            analysisresult = {'an': an}
            analysisresult['mass'] = str(mass)
            analysisresult['top'] = txnames[0]
            analysisresult['pid'] = str(PIDs)
            analysisresult['theo'] = theo
            analysisresult['ul'] = ulobs
            analysisresult['r'] = r
            if verbose:
                print(analysisresult)


        # append every result for the analysis
        results.append(analysisresult)
    return results

