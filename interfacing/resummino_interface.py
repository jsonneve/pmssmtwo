#!/usr/bin/env python3

from tools import text_manipulation as manip
import os
import subprocess
import locale
import itertools


def extract_xsecs(resummino_output):
    """
    Extract LO, NLO, NLL from resummino output.
    """
    resummino_xsecs = {}
    encoding = locale.getdefaultlocale()[1]
    if encoding == None or encoding == '':
        resum_out = str(resummino_output)
    else:
        resum_out = resummino_output.decode(encoding)
    # print( resum_out.split('\n'))
    lines = resum_out.split('\n')
    lo_index = [i for i in range(len(lines)) if lines[i].startswith('LO = ')]
    xsec_lines = [l for l in lines[lo_index[-1]:] if not l=='']
    #print(xsec_lines)
    #print(xsec_lines[2].split()[2].strip('('))
    lo, nlo, nll = [float(xsec_lines[i].split()[2].strip('(')) for i in range(3)]
    lo_err, nlo_err, nll_err = [float(xsec_lines[i].split()[4].strip(')')) for i in range(3)]
    resummino_xsecs['LO'] = lo
    resummino_xsecs['LO_err'] = lo_err
    resummino_xsecs['NLO'] = nlo
    resummino_xsecs['NLO_err'] = nlo_err
    resummino_xsecs['NLL'] = nll
    resummino_xsecs['NLL_err'] = nll_err
    return resummino_xsecs
    #print('LO', lo, '+-', lo_err, 'pb')
    #print('NLO', nlo, '+-', nlo_err, 'pb')
    #print('NLL', nll, '+-', nll_err, 'pb')


def resummino_input(slha, resummino_template, parts, outfile=None):
    """
    Write resummino input file.
    """
    if outfile == None:
        if '/' in resummino_template:
            outfile = resummino_template[resummino_template.rindex('/') + 1:]
        else:
            outfile = resummino_template
        outfile = outfile.replace('template', '').replace('.in', '')
        if '/' in slha:
            outfile += slha[slha.rindex('/') + 1:slha.rindex('.')]
        else:
            outfile += slha[:slha.rindex('.')]
        outfile += '_' + str(parts[0])
        outfile += '_' + str(parts[1])
        outfile += '.in'
        #print(outfile)
    reps = {'resummino_part1_in': parts[0]}
    reps['resummino_part2_in'] = parts[1]
    reps['resummino_slha_in'] = slha
    manip.write_and_replace(resummino_template, outfile, reps)
    return outfile


def run_resummino(resummino_dir, resummino_input, lo=False, verbose=False):
    """
    Run resummino in resumminodir on inputfile.
    Assumes executable is called resummino.
    """
    args = [os.path.join(resummino_dir, 'resummino')]
    args += [resummino_input]
    if lo:
        args += ['--lo']
    if verbose:
        print("Will run resummino with:", args)
    output = subprocess.check_output(args)
    if verbose:
        print("Ran resummino -- output:", output)
    return output


def neutralino_chargino_pairs():
    """ 
    Make all neutralino and chargino pairs for which production is allowed.
    That is, take care of charges and double counting.
    """
    # Make all combinations of charginos and neutralinos:
    neutralinos_and_charginos = [1000020 + i for i in range(2, 6)]
    neutralinos_and_charginos += [1000035, 1000037, -1000037, -1000024]
    particle_pairs = list(itertools.combinations_with_replacement(neutralinos_and_charginos, 2))

    # There is no same-sign chargino-chargino production though -- remove that:
    charginos = [1000024, 1000037]
    forbidden_chargino_pairs = list(itertools.combinations_with_replacement(charginos, 2))
    forbidden_chargino_pairs += list(itertools.combinations_with_replacement([-c for c in charginos], 2))

    for chargino_pair in forbidden_chargino_pairs:
        if chargino_pair in particle_pairs:
            particle_pairs.pop(particle_pairs.index(chargino_pair))
        else:
            particle_pairs.pop(particle_pairs.index((chargino_pair[1],
                chargino_pair[0])))
    return particle_pairs


def all_nn_xsecs(slha, resummino_dir, resummino_template, particle_pairs=[],
        LO=False, test=False):
    """
    Compute all nn cross sections (all combinations of
    charginos and neutralinos) with resummino at
    all orders for the given template with the executable
    in the given directory.

    >>> resummino_dir = "/afs/desy.de/user/s/sonnevej/xxl/resummino/bin"
    >>> resummino_template = os.path.abspath("resummino_template.in")
    >>> slha = os.path.abspath("pMSSM12_MCMC1_33_515635_small_nn_masses.slha")
    >>> all_nn_xsecs(slha, resummino_dir, resummino_template, [(1000022, 1000022)]
    {(1000022, 1000022): {'NLO': 0.0088051884, 'LO': 0.005390513, 'LO_err':
    7.1550203e-06, 'NLL': 0.0085539681, 'NLL_err': 9.9645809e-05, 'NLO_err':
        1.3235997e-05}}
    """
    xs = {}

    if particle_pairs in [[], None, 'all']:
        particle_pairs = neutralino_chargino_pairs()

    if test:
        particle_pairs = particle_pairs[:2]

    for parts in particle_pairs:
        resummino_in = resummino_input(slha, resummino_template, parts, outfile=None)
        resummino_summary = run_resummino(resummino_dir, resummino_in, lo=LO)
        xs[parts] = extract_xsecs(resummino_summary)
    return xs

if __name__ == "__main__":
    resummino_dir = "/afs/desy.de/user/s/sonnevej/xxl/resummino/bin"
    resummino_template = os.path.abspath("/afs/desy.de/user/s/sonnevej/pmssmtwo/interfacing/resummino_template.in")
    xs = {}
    slha = os.path.abspath("pMSSM12_MCMC1_33_515635_small_nn_masses.slha")
    neutralinos_and_charginos = [1000020 + i for i in range(2, 6)]
    neutralinos_and_charginos += [1000035, 1000037, -1000037, -1000024]
    #print(neutralinos_and_charginos)
    particle_pairs = list(itertools.combinations_with_replacement(neutralinos_and_charginos, 2))
    # There is no same-sign chargino-chargino production:
    for chargino in [1000024, 1000037]:
        particle_pairs.pop(particle_pairs.index((chargino, chargino)))
    #particle_pairs += [(ewikino, ewikino) for ewikino in neutralinos_and_charginos]
    for parts in particle_pairs[0:1]:
        #parts = (1000022, 1000022)
        print (parts)
        resummino_in = resummino_input(slha, resummino_template, parts, outfile=None)
        resummino_summary = run_resummino(resummino_dir, resummino_in)
        xs[parts] = extract_xsecs(resummino_summary)
        print( xs )

