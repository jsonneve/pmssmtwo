#!/usr/bin/env python3
import os
import subprocess
import math
import glob
import csv
import shutil as sh
from math import floor, log10
import datetime
import sys


"""
Use SModelS to compute NLLFast cross section.

"""

def compile_nllfast(smodelsdir):
    """
    Compile nllfast code in smodels.
    """
    curdir = os.getcwd()
    os.chdir(os.path.append(smodelsdir, 'lib'))
    args = ['make']
    if sys.version_info < (2, 7):
        subprocess.Popen(args)
    else:
        out = str(subprocess.check_output(args))
        if 'error' in out.lower():
            print("Could not compile nllfast. Please check your main file.")
            print(out)
            os.chdir(curdir)
            return None
    os.chdir(curdir)


def run_nllfast(smodelsdir, slha, order='nll', sqrtstev=13, make=False):
    """
    Calculate the next to leading log cross section for
    the given process for the given particle masses.
    Return the cross section in picobarn.
    """
    curdir = os.getcwd()
    os.chdir(smodelsdir)
    oderarg = '-N'
    if order.lower() == 'nll':
        orderarg = '-N'
    elif order.lower() =='nlo':
        orderarg = '-n'
    else:
        orderarg = ''

    # Compile prospino.
    if make:
        if compile_nllfast(smodelsdir) == None:
            return None

    # Put slhafile in place:
    prospino_slhafile = os.path.join(prospinodir, "prospino.in.les_houches")
    if slha != None:
        sh.copyfile(slha, prospino_slhafile)
    else:
        print("Assuming", prospino_slhafile, "is already in place.")
    args = ['./runTools.py','xseccomputer', orderarg, '-s', str(sqrtstev), '-f', slha]
    out = str(subprocess.check_output(args))
    if 'error' in out.lower():
        print("Found error -- please check nllfast dir")
        return None
    os.chdir(curdir)
    return out


def smodels_nll_to_dict(smodelsoutput):
    """
    Put smodels output in nll dict.
    """
    pass

# output example:
# 13 TeV (NLO+NLL)   (1000001, 1000003):  3.029e-03 pb