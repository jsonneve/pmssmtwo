#!/usr/bin/env python3

"""
Interface to prospino for NLO computations.
Useful when sparticle masses are outside nllfast grid.

"""

import os
import subprocess
import math
import glob
import csv
import shutil as sh
from math import floor, log10
import datetime
import sys
import pyslha
import resummino_interface as resummino
import itertools


def read_particle_masses(slha):
    """
    Read masses from slha.
    These can be used to check for
    example if sqrts is large enough
    to produce two gluinos or if the
    neutralino and chargino masses of the
    produced particles are larger than
    the Z mass (in these cases prospino fails).
    """
    d = pyslha.readSLHAFile(slha)
    masses = d.blocks['MASS']
    if 23 not in masses: # Z mass
        masses[23] = d.blocks['SMINPUTS'][4]
    return masses


def compile_prospino(prospinodir, prospino_main=None):
    """
    Compile prospino code.
    """
    curdir = os.getcwd()
    prospino_mainfile = os.path.join(prospinodir, "prospino_main.f90")
    if prospino_main != None:
        sh.copyfile(prospino_main, prospino_mainfile)
    else:
        print("Assuming", prospino_mainfile, "is already in place.")
    os.chdir(prospinodir)
    args = ['make']
    fortran_binaries = glob.glob('*.o') + glob.glob('*.mod')
    for binary in fortran_binaries:
        os.remove(binary)
    if sys.version_info < (2, 7):
        subprocess.Popen(args)
    else:
        out = str(subprocess.check_output(args))
        if 'error' in out.lower():
            print("Could not compile prospino. Please check your main file.")
            print(out)
            os.chdir(curdir)
            return None
    os.chdir(curdir)

def run_prospino(prospinodir, prospino_main=None, slha=None, sqrts=13,
        make_prosp=False, particle_masses=None, testing=False):
    """
    Calculate the next to leading log cross section for
    the given process for the given particle masses.
    Return the cross section and its upper and lower
    limits (due to scale variation) in picobarn.
    """
    # Put slhafile in place:
    prospino_slhafile = os.path.join(prospinodir, "prospino.in.les_houches")
    if slha != None:
        sh.copyfile(slha, prospino_slhafile)
    else:
        print("Assuming", prospino_slhafile, "is already in place.")

    # Change to prospino directory
    curdir = os.getcwd()
    os.chdir(prospinodir)

    # Compile prospino.
    if make_prosp:
        if compile_prospino(prospinodir, prospino_main) == None:
            return None

    args = ['./prospino_2.run']
    if testing:
        args = ['./prospino_2_test.run']
    if particle_masses == None:
        particle_masses = read_particle_masses(prospino_slhafile)

    # Check that masses are reachable at sqrts:
    if 2*particle_masses[1000021] > sqrts*1000.0: # sqrts in TeV, mass in GeV!
        # If energy is not enough to produce gluino
        # prospino crashes. Using my prospino_main.f90
        # with the argument mgobad prevents computation of
        # p p > go go so that the rest gets computed:
        args += ['mgobad']

    # Check that neutralino/chargino masses add up to > mZ:
    neutralinos_and_charginos = [1000020 + i for i in range(2,6)]
    neutralinos_and_charginos += [1000035, 1000037]
    combos = [(nch, nch) for nch in neutralinos_and_charginos]
    combos += list(itertools.combinations(neutralinos_and_charginos, 2))
    for n1, n2 in combos:
        mass_sum_nch = abs(particle_masses[n1]) + abs(particle_masses[n2])
        mz = particle_masses[23]
        if mass_sum_nch < mz:
            # In this case prospino would give a hard stop and say:
            # 'IFCT_NN_X12: masses low, Z decays might be more suitable'
            # This means that the sum of neutralino/chargino masses lower
            # than the Z mass.
            # Using my prospino_main.f90 with the argument mnebad prevents 
            # computation of p p > go go so that the rest gets computed:
            args += ['mnebad']
            break

    # You need a version of python >=2.7 to use 'check_output' of subprocess:
    if verbose:
        print("will call prospino with:", args)

    out = str(subprocess.check_output(args))
    if 'HARD_STOP' in out or 'error' in out.lower():
        if ' ENERGY TOO SMALL TO PRODUCE' in out:
            print("Not enough energy to produce particles. Will take zero cross section.")
            print("Not yet fixed; skipping this file")
            return None
        #    print("Skipping this file because of nn")
        #    return None
        #    #skip = 'nn'
        #    #prospino_orig, prospino_main = replace_prospino_skip([skip], prospinodir)
        #    #compile_prospino(prospinodir, prospino_main)
        #    #out = str(subprocess.check_output(['./prospino_2.run']))
        #    #sh.copyfile(prospino_orig, prospino_main)
        else:
            print("There was an error in running prospino. Please check your main and slha file.")
            print(out)
    output = os.path.abspath('prospino.dat')
    os.chdir(curdir)
    return output


def read_prospino_xsecs(prospino_output):
    """
    Given a prospino_main.f90 file (prospino_main),
    run it for the given slha file and read out
    resulting prospino.dat file.
    This assumes that the desired energy and processes are
    switched on in the prospino_main.f90 file.

    Cross sections for neutralino-squark are given at LO only
    since NLO is not implemented/checked yet.

    If sum_process is true, squarks and sleptons will be summed
    over and only total cross sections for all processes
    will be given.
    If not, cross sections will be given for process+particles
    in prospino language.
    Use translator to get cross sections in PIDs.

    """
    # In case prospino failed:
    if prospino_output == None: return {}, []

    # "i1 i2  dummy0 dummy1 scafac  m1    m2      angle LO[pb]   rel_error NLO[pb]   rel_error   K    LO_ms[pb] NLO_ms[pb]"
    prospino_headers = "proc i1 i2  dummy0 dummy1 scafac  m1    m2      angle    LO[pb]   rel_error_lo  NLO[pb]  rel_error_nlo   K    LO_ms[pb] NLO_ms[pb]".split()
    full_info_xsecs = []
    lo_ms = 'LO_ms[pb]'
    nlo_ms = 'NLO_ms[pb]'
    rel_error_lo = 'rel_error_lo'
    rel_error_nlo = 'rel_error_nlo'
    xsec_dict = {}
    with open(prospino_output, 'r') as output:
        dictreader = csv.DictReader(output, fieldnames=prospino_headers, delimiter=' ', skipinitialspace=True)
        #line = output.readline()
        for row in dictreader:
            if row['i1'] == 'i2': break # you arrived at the header line (bottom); stop here
            full_info_xsecs.append(row)
            if row['proc'] in xsec_dict:
                for qty in [lo_ms, nlo_ms, rel_error_lo, rel_error_nlo]:
                    xsec_dict[row['proc']][qty] += float(row[qty])
            else:
                xsec_dict[row['proc']] = {}
                for qty in [lo_ms, nlo_ms, rel_error_lo, rel_error_nlo]:
                    xsec_dict[row['proc']][qty] = float(row[qty])

    # Compute total cross sections
    total_xsec_lo = sum([xsec_dict[pr][lo_ms] for pr in xsec_dict])
    # Add relative errors in quadrature:
    total_xsec_lo_rel_err = math.sqrt(sum([xsec_dict[pr][rel_error_lo]**2 for pr in xsec_dict]))
    total_xsec_nlo_rel_err = math.sqrt(sum([xsec_dict[pr][rel_error_nlo]**2 if pr != 'ns' else xsec_dict[pr][rel_error_lo] for pr in xsec_dict]))
    # Add all cross sections for total
    total_xsec_nlo = sum([xsec_dict[pr][nlo_ms] if pr != 'ns' else xsec_dict[pr][lo_ms] for pr in xsec_dict])

    # Cross sections for neutralino-squark are given at LO only
    # since NLO is not implemented/checked yet.
    if xsec_dict != {}:
        # Take care that any information was read out.
        # If file was empty, return just an empty dictionary.
        xsec_dict['tot'] = {}
        xsec_dict['tot'][nlo_ms] = total_xsec_nlo
        xsec_dict['tot'][rel_error_nlo] = total_xsec_nlo_rel_err

        xsec_dict['tot'][lo_ms] = total_xsec_lo
        xsec_dict['tot'][rel_error_lo] = total_xsec_lo_rel_err

    return xsec_dict, full_info_xsecs



def make_line(d, headers=None, sep="|", end="\n"):
    """(dict, str, str -> str)
    Make a string from the given dictionary.
    Return a line with given line ending.

    """
    if headers == None: headers = sorted(d.keys())
    return "".join([str(d[k]) + sep for k in headers]) + end

def format_line(d, template=None, headers=None, widths=10, end="\n"):
    """(dict, list, int, str)->str
    Format dict nicely into line for table
    with fixed widths for entries.
    """

    # define template:
    if template == None:
        if headers == None:
            # define headers
            headers = d.keys()
        if not is_instance(widths, list):
            # define width for each cell:
            widths=[widths]*len(headers)
        template = "".join(["{" + headers[i] + ":" + str(widths[i]) + "}" for i in range(len(headers))])
    #template = "".join(["{" + proc + ":10}{" + proc + '_rel_err:10}' for proc in sorted(xsecs.keys())])
    #template = "{id:30}" + template
    #header = {qt: qt for qt in headers}
    #print(template.format(**header)) # header
    #for nlo_summary in nlo_summaries:
    return template.format(**d) + end

def append_to_file(filename, string):
    """
    Append given string to filename.
    """
    f = open(filename, 'a')
    f.write(string)
    f.close()

def round_to_sign(x, sig=3):
    """
    Round the given number to the significant number of digits.
    """
    if math.isnan(x): return x
    if x <= 0: return x
    return round(x, sig-int(floor(log10(x)))-1)

def prospino_dict(resummino_out, ew, particle_masses=None, lo_if_nan=True):
    """
    Write resummino output in prospino dictionary form.
    """
    pdict = {}
    print pheader_entries
    # Initialize all entries for prospino to -1:
    ### Python 3 ###
    # pdict = {h: -1 for h in pheader_entries}
    ### End Python 3 ###
    ### Python2 ####
    for h in pheader_entries:
        pdict[h] = -1
    ### End Python 2 ###

    pdict['proc'] = 'nn'

    # The corresponding name of the entry in prospino is indicated
    # for every added entry of the prospino line (p_line)
    pdict['i1'] = str(ew[0]) # part1 in = i1
    pdict['i2'] = str(ew[1]) # part2 in = i2

    # NLO = dummy0
    nlo = resummino_out['NLO']
    pdict['dummy0'] = str(nlo)
    # NLO_rel_err = dummy1
    if nlo != 0:
        pdict['dummy1'] = str(resummino_out['NLO_err']/nlo)

    # LO = LO_ms[pb]
    lo = resummino_out['LO']
    pdict['LO_ms[pb]'] = str(lo)
    if lo != 0:
        pdict['K'] = nlo/lo
        pdict['rel_error'] = str(resummino_out['LO_err']/lo)
    nll = resummino_out['NLL']
    # in case resummino failed, take lo!
    if math.isnan(nll) and lo_if_nan:
        nll = lo
    # NLL = NLO_ms[pb]
    pdict['NLO_ms[pb]'] = str(nll)

    #NLL_rel_err = rel_error NLO[pb]
    if nll != 0:
        # if resummino failed, take lo only:
        if math.isnan(nll) and lo_if_nan:
            pdict['rel_error_NLO[pb]'] = str(resummino_out['LO_err']/lo)
        else:
            pdict['rel_error_NLO[pb]'] = str(resummino_out['NLL_err']/nll)
    if particle_masses != None:
        pdict['m1'] = particle_masses[abs(ew[0])]
        pdict['m2'] = particle_masses[abs(ew[1])]
    return pdict


if __name__ == "__main__":


    # Print more info with verbose:
    verbose = False
    testing = False
    compile_prosp = False

    # Define prospino directory, main file, slha directory
    # prospino_dir = '/nfs/dust/cms/user/sonnevej/prospino/on_the_web_10_17_14'
    # prospino_dir = '/nfs/dust/cms/user/sonnevej/prospino/on_the_web_10_17_14'
    prospino_dir = '../../prospino/on_the_web_10_17_14'
    # prospino_main = os.path.abspath('prospino_main.f90')
    # prospino_main = '/nfs/dust/cms/user/sonnevej/prospino/prospino_main.f90'
    prospino_main = 'prospino_main.f90'

    # for testing: includes only nn and gg
    if testing:
        prospino_main = os.path.abspath('prospino_main_test.f90')


    # Resummino executable directory:
    #resummino_dir = "/nfs/dust/cms/user/sonnevej/resummino/bin"
    resummino_dir = "../../resummino/bin"
    # Resummino template:
    resummino_template = os.path.abspath("resummino_template.in")



    args = sys.argv
    print("arguments given:", args)

    if 'testing' in args or 'test' in args:
        testing = True
        print("Running in mode TESTING")
    if 'compile' in args or 'compileprospino' in args or 'compile_prosp' in args or 'compile_prospino' in args:
        compile_prosp = True
        print("Will compile prospino")
    if 'verbose' in args or '-v' in args:
        verbose = True
        print("Running in verbose mode")


    if len(args) > 1:
        slhafiles = [args[1]]
    else:
        slhafiles = glob.glob("/nfs/dust/cms/user/sonnevej/prospino/slha/*.slha")
        #if verbose: print("Will start running over slhafiles in:", slhafiles[0][:slhafiles[0].rindex('/')])
    # second is prospino outfile
    if len(args) > 3:
        # Define output file
        # separated by |
        outfile = args[3]
    else:
        outfile = "/nfs/dust/cms/user/sonnevej/prospino/prospino_results"
    if len(args) > 4:
        # separated by space
        outfile2 = args[4]
    else:
        outfile2 = "/nfs/dust/cms/user/sonnevej/prospino/prospino_results_nice"


    if verbose:
        # Gather all information in a list that will be printed at end if desired:
        nlo_summaries = []
        # print failed files at end:
        failed_files = []

    # Write date and time to file
    date_and_time = datetime.datetime.isoformat(datetime.datetime.today())
    append_to_file(outfile, date_and_time + '\n')
    append_to_file(outfile2, date_and_time + '\n')

    # Compile prospino only once:
    if compile_prosp:
        prosp_out = compile_prospino(os.path.abspath(prospino_dir), prospino_main)
        if verbose: print("Compiling prospino code", prospino_main, "in", prospino_dir, '\n...')

    # Processes computed by prospino:
    processes = ['ng', 'bb', 'gg', 'll', 'nn', 'ns', 'sb', 'sg', 'ss', 'tb']

    # names or relative errors of cross sections:
    proc_rel_errs = ['ng_rel_err', 'bb_rel_err', 'gg_rel_err', 'id_rel_err', 'll_rel_err']
    proc_rel_errs += ['nn_rel_err', 'ns_rel_err', 'sb_rel_err', 'sg_rel_err']
    proc_rel_errs += ['ss_rel_err', 'tb_rel_err']

    # All entries that are saved (header names):
    headers = ['id'] + processes + ['tot', 'tot_rel_err'] + proc_rel_errs + ['time']

    # For nice output formatting:
    ### Python2 ####
    header = {}
    for qt in headers:
        header[qt] = qt
    ### End Python2 ####
    ### Python 3 ###
    #header = {qt: qt for qt in headers}
    ### End Python 3 ###
    template = "".join(["{" + headers[i] + ":13}" for i in range(len(headers)) if headers[i] != 'id'])
    template = "{id:30}" + template
    print(template)
    # Write headers to file:
    append_to_file(outfile, make_line(header, headers))
    append_to_file(outfile2, format_line(header, template))


    # Run over slhafiles:
    if verbose:
        print(format_line(header,template))
    if testing and len(slhafiles) > 3:
        slhafiles = [fi for fi in slhafiles if 'pMSSM12_MCMC1_33_515635_zmassproblem.slha' in fi]
    # slhafiles = [fi for fi in slhafiles if 'mg5_default.slha' in fi]

    print("will run over slhafiles:", slhafiles)
    for slha in slhafiles:

        # Make NLO summary (ignoring LO), using default xsecs/errors of -1:
        ### Python 3 ###
        # nlo_summary = {h: -1 for h in headers}
        ### End Python 3 ###
        ### Python2 ####
        nlo_summary = {}
        for h in headers:
            nlo_summary[h] = -1
        ### End Python2 ####
        # Save id name (overwrite the -1):
        slha_name = slha.split('/')[-1].strip('.slha')
        nlo_summary['id'] = slha_name
        nlo_summary['time'] = date_and_time

        # Run prospino
        if verbose: 
            print("Running prospino on", slha_name, "located at", slha)
            print("...")

        particle_masses = read_particle_masses(slha)
        if 'prospino_input_ready' in args:
            prosp_out = os.path.abspath('../../prospino/on_the_web_10_17_14/prospino.dat')
        else:
            prosp_out = run_prospino(os.path.abspath(prospino_dir),
                    prospino_main=None, slha=slha, sqrts=13, make_prosp=False,
                    particle_masses=particle_masses, testing=testing)
        # If prospino failed, no output is returned (None)
        # Reading xsecs below will return empty dictionaries in that case

        # Read cross sections from prospino output:
        xsecs, xsecs_full = read_prospino_xsecs(prosp_out)

        if xsecs == {}:

            if verbose:
                print("Could not run on this slha file:", slha)
                print("Please check prospino output.")
                print(nlo_summary)
                print(format_line(nlo_summary, template))
                failed_files.append(slha)
            # Store cross sections of -1 (set by default above)
            append_to_file(outfile, make_line(nlo_summary, headers))
            append_to_file(outfile2, format_line(nlo_summary, template))
            continue

        if verbose:
            print("Ran prospino:", xsecs)
            print("Ran prospino:", xsecs_full)


        # It could be that the 'nn' cross sections failed.
        # This is the case when e.g. mn1 + mn2 < mZ (with n=neutralino,
        # chargino). In this case, call resummino:
        electroweak = {}

        ######## Write to resummino output to prospino file #######
        psep = '  '
        # write in this format (one line):
        # gg  0  0     0.0    0.0    1.0 2237.7 2237.7  0.000 0.813E-04
        #     i1 i2  dummy0 dummy1 scafac  m1    m2      angle LO[pb]
        # 0.842E-03 0.232E-03 0.964E-03 2.8568 0.817E-04 0.234E-03
        # rel_error NLO[pb]   rel_error   K    LO_ms[pb] NLO_ms[pb]
        pheaders = '    i1 i2  dummy0 dummy1 scafac  m1    m2      angle        LO[pb]'
        pheaders += '  rel_error NLO[pb]   rel_error_nlo   K    LO_ms[pb] NLO_ms[pb]'
        pheader_entries = ['proc'] + pheaders.split()

        if 'nn' not in xsecs or xsecs['nn'] == -1:
            particle_pairs = resummino.neutralino_chargino_pairs()
            if 'testing_resummino' in args:
                #pass
                particle_pairs=particle_pairs[:2]
            for parts in particle_pairs:
                resummino_in = resummino.resummino_input(slha, resummino_template, parts)
                resummino_summary = resummino.run_resummino(resummino_dir,
                        resummino_in, lo=testing, verbose=verbose)
                electroweak[parts] = resummino.extract_xsecs(resummino_summary)
                pdict = prospino_dict(electroweak[parts], parts,
                        particle_masses=particle_masses)
                print pdict
                append_to_file(prosp_out, make_line(pdict, pheader_entries,
                    sep=psep))
            # aggregate
            """
            # when running all at once:
            electroweak = resummino.all_nn_xsecs(slha, resummino_dir,
                    resummino_template, LO=testing, test=testing)
            """
            nn = sum(electroweak[i]['NLL'] for i in list(set(electroweak.keys())))
            nn_rel_err2 = sum(electroweak[i]['NLL_err']**2/electroweak[i]['NLL']**2 for i in
                    list(set(electroweak.keys())) if electroweak[i]['NLL'] != 0)
            # testing:
            if testing:
                nn = sum(electroweak[i]['LO'] for i in list(set(electroweak.keys())))
                nn_rel_err2 = sum(electroweak[i]['LO_err']**2/electroweak[i]['LO']**2 for i in
                     list(set(electroweak.keys())) if electroweak[i]['LO'] != 0)
            nlo = 'NLO_ms[pb]'
            nlo_rel_err = 'rel_error_nlo'
            xsecs['nn'] = {nlo: nn} # use prospino name, add nll anyway to nlo
            xsecs['nn'][nlo_rel_err] = math.sqrt(nn_rel_err2) # again use nlo for what is actually NLL
            xsecs['tot'][nlo] += nn
            xsecs['tot'][nlo_rel_err] = math.sqrt(xsecs['tot'][nlo_rel_err]**2
                    + nn_rel_err2)
            if verbose:
                print("Ran resummino -- output:")
                print(electroweak)









        # Store NLO cross sections (WARNING: ns is at LO!) in python dict:
        for proc in xsecs:

            proc_nlo = proc
            proc_err = proc + '_rel_err'

            # Skip NLO for ns (not implemented in prospino):
            if proc == 'ns':
                nlo_summary[proc_nlo] = round_to_sign(xsecs[proc]['LO_ms[pb]'], 3)
                nlo_summary[proc_err] = round_to_sign(xsecs[proc]['rel_error_lo'], 3)
            else:
                nlo_summary[proc_nlo] = round_to_sign(xsecs[proc]['NLO_ms[pb]'], 3)
                nlo_summary[proc_err] = round_to_sign(xsecs[proc]['rel_error_nlo'], 3)
        # Store cross sections to file:
        append_to_file(outfile, make_line(nlo_summary, headers))
        append_to_file(outfile2, format_line(nlo_summary, template))
        if verbose:
            print("Ran prospino -- output:")
            print(format_line(nlo_summary, template))
            nlo_summaries.append(nlo_summary)

    if verbose:
        print(80*'=')
        print("")
        print("Failed to compute cross sections for the following files:")
        for f in failed_files:
            print(f)

        print(80*'=')
        print("")
        print("You now have a python dictionary with NLO xsecs in it.")
        print("Note that the xsec for ns is NOT NLO, as this is not yet implemented in prospino.")
        print("For the total NLO cross section the part from neutralino-squark is only at LO.")
        print(nlo_summaries)


        print(80*'=')
        print("")
        print("")
        # Print results
        template = "".join(["{" + proc + ":10}{" + proc + '_rel_err:10}' for proc in headers if proc != 'id'])
        template = "".join(["{" + proc + ":10}{" + proc + '_rel_err:10}' for proc in sorted(xsecs.keys())])
        template = "{id:30}" + template
        #header = {qt: qt for qt in headers}
        print(template.format(**header)) # header
        for nlo_summary in nlo_summaries:
            print(template.format(**nlo_summary))

    if len(sys.argv) > 2:
        prospino_outfile = args[2]
        sh.copyfile(prosp_out, prospino_outfile)
