#!/usr/bin/env python3

"""
Interface to nllfast.

"""

import os
import subprocess
import math
from modelspecific import readmasses
import glob
import datetime
import sys
from prospino_interface import format_line, make_line
from prospino_interface import append_to_file
from prospino_interface import round_to_sign


def nll_gosq(nlldir, mgo, msq, pdf='cteq', proc='sg'):
    """
    Calculate the next to leading log cross section for
    squark-gluino associate production for the given
    squark and gluino masses.
    Return the cross section and its upper and lower
    limits (due to scale variation) in picobarn.
    """
    return nll(nlldir, proc, [mgo, msq], pdf)

def nll(nlldir, proc, masses, pdf='cteq'):
    """
    Calculate the next to leading log cross section for
    the given process for the given particle masses.
    Return the cross section and its upper and lower
    limits (due to scale variation) in picobarn.

    >>> nllfast_dir = '/afs/desy.de/user/s/sonnevej/xxl/nllfast/nllfast-3.1'
    >>> msb1 = 1.51313030E+03
    >>> msb2 = 2.24972613E+03
    >>> msb = (msb1 + msb2)/2.
    >>> nll(nllfast_dir, 'st', ['msb1'])
    """ 
    massstrings = [str(mass) for mass in masses]
    curdir = os.getcwd()
    os.chdir(nlldir)
    args = ['./nllfast', proc, pdf] + massstrings
    out = subprocess.check_output(args)
    output = proc + '.out'
    if 'error' in out.lower() or 'too low/high' in out.lower():
        print("Could not run nllfast on", proc, masses)
        print(out)
        return None
    nll_pos = [len(masses) + i for i in [2, 3, 4]]
    xsec, dxsplus, dxsmin = (float(open(output, 'r').readlines()[-1].split()[i]) for i in nll_pos)
    os.chdir(curdir)
    return xsec, dxsplus, dxsmin

def nll_stops(nlldir, mstop, pdf='cteq', proc='st'):
    """
    Calculate the next to leading log cross section for
    stop-antistop production for the given stop mass.
    Return the cross section and its upper and lower
    limits (due to scale variation) in picobarn.
    """
    return nll(nlldir, proc, [mstop], pdf)


def total_xsec(nlldir, slha, pdf='nnpdf'):
    """
    Compute the total cross section,
    do not average over squark masses.
    If outside the grid return cross sections of -1.

    >>> nllfast_dir = '/afs/desy.de/user/s/sonnevej/xxl/nllfast/nllfast-3.1'
    >>> testfile = "pMSSM12_MCMC1_13_655251.slha"
    >>> total_xsec(nllfast_dir, test_file)
    """

    total_xsec_nlo_ms_pb = 0
    totxs_nloms_pb_errp_sq = 0
    totxs_nloms_pb_errm_sq = 0
    masses = pyslha.readSLHAFile(slha).blocks['MASS']
    keys =  ['gg', 'sg', 'ss', 'sb']
    rsquarks = [2000001 + i for i in range(4)]
    lsquarks = [1000001 + i for i in range(4)]
    squarks = rsquarks + lsquarks
    keys =  ['gg', 'sg', 'ss', 'sb']
    procs = {'gg': [[masses[1000021], masses[1000021]]]}
    procs['sg'] = [[masses[m2], masses[1000021]] for m2 in squarks]}
    procs['ss'] = [[masses[m2], masses[m2]] for m2 in squarks]]
    # here you need lo xsecs
    # get individual kfactors from nllfast and multiply with LO for
    # each individual particle (NLLFast sums over all 5 light-flavor
    # squarks!).



def total_xsec_avg_mass(nlldir, slha, pdf='nnpdf'):
    """
    Compute the total cross section at NLL with
    masses in the slha file.
    Average over squark masses.
    If outside grid return None.

    >>> nllfast_dir = '/afs/desy.de/user/s/sonnevej/xxl/nllfast/nllfast-3.1'
    >>> testfile = "pMSSM12_MCMC1_13_655251.slha"
    >>> total_xsec(nllfast_dir, test_file)
    """
    total_xsec_nlo_ms_pb = 0
    totxs_nloms_pb_errp_sq = 0
    totxs_nloms_pb_errm_sq = 0
    masses = readmasses.read_mass(slha)

    keys =  ['gg', 'sg', 'ss', 'sb']
    #if sys.version_info < (2, 7):
    nlldict = dict([(k, -1) for k in keys])
    #else:
    #    nlldict = {k: -1 for k in keys}
    for gosq in keys:
        nllresult = nll(nlldir, gosq, [masses['mgo'], masses['msq']], pdf)
        if nllresult != None:
            xs, xsp, xsm = nllresult
            nlldict[gosq] = xs
            if xs > 0 and (xsp > 0 or xsm > 0):
                nlldict[gosq+'_rel_err'] = round_to_sign((xsp + abs(xsm))/2., 3)
            else:
                nlldict[gosq + '_rel_err'] = 0
            #total_xsec_nlo_ms_pb += xs
            totxs_nloms_pb_errp_sq += xsp**2
            totxs_nloms_pb_errm_sq += xsm**2
    for proc in ['tb'], 'bb']:# bb is is summed over above?
        if proc == 'bb': thirdgen = masses['msbottom']
        if proc == 'tb': thirdgen = masses['mstop']
        nlldict[proc] = -1
        nlldict[proc + '_rel_err'] = -1
        # Note that NLLFast should not be used for mstop > mgluino:
        if thirdgen > masses['mgo']: continue
        nllresult = nll_stops(nlldir, thirdgen, pdf)
        if nllresult != None:
            xs, xsp, xsm = nllresult
            nlldict[proc] = xs
            if xs > 0 and (xsp > 0 or xsm > 0):
                nlldict[proc+'_rel_err'] = round_to_sign((xsp + abs(xsm))/2., 3)
            else:
                nlldict[proc + '_rel_err'] = 0
            #total_xsec_nlo_ms_pb += xs
            totxs_nloms_pb_errp_sq += xsp**2
            totxs_nloms_pb_errm_sq += xsm**2
            #totxs_nloms_pb_errp = math.sqrt(totxs_nloms_pb_errp, xsp)
            #totxs_nloms_pb_errm = math.sqrt(totxs_nloms_pb_errm, xsm)

    total_xsec_nlo_ms_pb = sum([nlldict[k] for k in nlldict if nlldict[k] > 0
        and 'rel' not in k])
    nlldict['tot'] = total_xsec_nlo_ms_pb
    errp = math.sqrt(totxs_nloms_pb_errp_sq)
    errm = math.sqrt(totxs_nloms_pb_errm_sq)
    rel_err = round_to_sign((errp + errm)/2., 3)
    nlldict['tot_rel_err'] = rel_err
    return nlldict


if __name__ == "__main__":


    # Print more info with verbose:
    verbose = True

    # Define nllfast directory, main file, slha directory
    nllfast_dir = '/afs/desy.de/user/s/sonnevej/xxl/nllfast/nllfast-3.1'
    slhafiles = glob.glob("/nfs/dust/cms/user/sonnevej/nllfast/slha/*.slha")

    # Define output file
    # separated by |
    outfile = "/nfs/dust/cms/user/sonnevej/nllfast/nllfast_results"
    # separated by space
    outfile2 = "/nfs/dust/cms/user/sonnevej/nllfast/nllfast_results_nice"


    if verbose:
        # Gather all information in a list that will be printed at end if desired:
        nlo_summaries = []
        # print failed files at end:
        failed_files = []

    # Write date and time to file
    date_and_time = datetime.datetime.isoformat(datetime.datetime.today())
    append_to_file(outfile, date_and_time + '\n')
    append_to_file(outfile2, date_and_time + '\n')


    # Processes computed by nllfast:
    processes = ['ng', 'bb', 'gg', 'll', 'nn', 'ns', 'sb', 'sg', 'ss', 'tb']
    # tt?

    # names or relative errors of cross sections:
    proc_rel_errs = ['bb_rel_err', 'gg_rel_err', 'id_rel_err', 'll_rel_err']
    proc_rel_errs += ['nn_rel_err', 'ns_rel_err', 'sb_rel_err', 'sg_rel_err']
    proc_rel_errs += ['ss_rel_err', 'tb_rel_err']

    # All entries that are saved (header names):
    headers = ['id'] + processes + ['tot', 'tot_rel_err'] + proc_rel_errs

    # For nice output formatting:
    ### Python2 ####
    header = {}
    for qt in headers:
        header[qt] = qt
    ### End Python2 ####
    ### Python 3 ###
    #header = {qt: qt for qt in headers}
    ### End Python 3 ###
    template = "".join(["{" + headers[i] + ":13}" for i in range(len(headers)) if headers[i] != 'id'])
    template = "{id:30}" + template
    # Write headers to file:
    append_to_file(outfile, make_line(header, headers))
    append_to_file(outfile2, format_line(header, template))


    # Run over slhafiles:
    if verbose: print("Will start running over slhafiles in:", slhafiles[0][:slhafiles[0].rindex('/')])
    if verbose:
        print(format_line(header,template))
    for slha in slhafiles:

        # Make NLO summary (ignoring LO), using default xsecs/errors of -1:
        ### Python 3 ###
        # nlo_summary = {h: -1 for h in headers}
        ### End Python 3 ###
        ### Python2 ####
        nlo_summary = {}
        for h in headers:
            nlo_summary[h] = -1
        ### End Python2 ####
        # Save id name (overwrite the -1):
        slha_name = slha.split('/')[-1].strip('.slha')
        nlo_summary['id'] = slha_name

        # Run nllfast
        if verbose: 
            print("Running nllfast on", slha_name, "located at", slha)
            print("...")
        out = total_xsec(nllfast_dir, slha)
        print(out)
        nlo_summary.update(out)
        # If nllfast failed, cross sections of -1 are returned.
        if -1 in out.values():
            if verbose:
                print("Could not run on this slha file:", slha)
                print("Please check nllfast output.")
                print(nlo_summary)
                print(format_line(nlo_summary, template))
                failed_files.append(slha)
            # Store cross sections of -1 (set by default above)
            append_to_file(outfile, make_line(nlo_summary, headers))
            append_to_file(outfile2, format_line(nlo_summary, template))
            continue
        if verbose:
            print("Ran nllfast -- output:")
            print(format_line(nlo_summary, template))
            nlo_summaries.append(nlo_summary)
        append_to_file(outfile, make_line(nlo_summary, headers))
        append_to_file(outfile2, format_line(nlo_summary, template))

    if verbose:
        print(80*'=')
        print("")
        print("Failed to compute cross sections for the following files:")
        for f in failed_files:
            print(f)

        print(80*'=')
        print("")
        print("You now have a python dictionary with NLO xsecs in it.")
        print("Note that the xsec for ns is NOT NLO, as this is not yet implemented in nllfast.")
        print("For the total NLO cross section the part from neutralino-squark is only at LO.")
        print(nlo_summaries)


        print(80*'=')
        print("")
        print("")
        # Print results
        template = "".join(["{" + proc + ":10}{" + proc + '_rel_err:13}' for
            proc in headers if proc != 'id' and not 'rel_err' in proc])
        #template = "".join(["{" + proc + ":10}{" + proc + '_rel_err:10}' for proc in sorted(xsecs.keys())])
        template = "{id:30}" + template
        #header = {qt: qt for qt in headers}
        print(template.format(**header)) # header
        for nlo_summary in nlo_summaries:
            print(template.format(**nlo_summary))
