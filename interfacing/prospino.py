#!/usr/bin/env python3

"""
Interface to prospino.

"""

import os
import subprocess
import math
import sys
import shutil as sh
sys.path.append('..')
from modelspecific import readmasses

def prospino_sum(prospino_dir, prospino_main, slha):
    """
    Run prospino on all particles.
    Use existing given prospino_main.f90 for this.
    Run on given slha.
    Read out all cross sections and return the sum.

    >>> pdir = os.path.abspath("../../bin/prospino")
    >>> testf90 = "prospino_gg13.f90"
    >>> testslha = "test_gg_t1_500_100.slha"
    >>> nlo_ms, lo_ms, rel_err = prospino_sum(pdir, testf90, testslha)
    >>> from interfacing import nllfast_interface as nllf
    >>> nlldir = os.path.abspath("~/bin/nllfast13")
    >>> xs, sxp, xsm = nllf.nll_gosq(nlldir, 500, 1e5, 'nnpdf', 'gg')
    >>> xsm < nlo_ms
    True
    >>> xsp*1.1 > nlo_ms
    True

    """

    sh.copyfile(slha, os.path.join(prospino_dir, "prospino.in.les_houches"))
    sh.copyfile(prospino_main, os.path.join(prospino_dir, "prospino_main.f90"))
    os.chdir(prospino_dir)
    out = subprocess.check_output(['make'])
    out = subprocess.check_output(['./prospino_2.run'])
    # output written to prospino.dat
    # output looks like
    #tb  2  0     0.0    0.0    1.0 2557.2 2557.2 -0.836 0.293E-06 0.789E-03
    #0.867E-06 0.558E-03 2.9575 0.293E-06 0.867E-06

    #    i1 i2  dummy0 dummy1 scafac  m1    m2      angle LO[pb]   rel_error
    #    NLO[pb]   rel_error   K    LO_ms[pb] NLO_ms[pb]
    fopen = open('prospino.dat')
    line = fopen.readline()
    nlo_ms = lo_ms = rel_err_squared = 0
    while line != '\n' and not 'dummy' in line and line != "":
        nlo_ms += float(line.split()[-1])
        lo_ms += float(line.split()[-2])
        rel_err_squared += float(line.split()[-4])**2
    return nlo_ms, lo_ms, math.sqrt(rel_err_squared)



if __name__ == "__main__":
    import doctest
    doctest.testmod()


