program main
  use xx_kinds
  use xx_prospino_subroutine
  implicit none

  integer                              :: inlo,isq_ng_in,icoll_in,i_error_in,ipart1_in,ipart2_in,isquark1_in,isquark2_in
  real(kind=double)                    :: energy_in
  logical                              :: lfinal
  logical                              :: do_mgo
  logical                              :: do_mneu
  character(len=2)                     :: final_state_in
  character(len=6)                     :: mgo_bad
  character(len=6)                     :: mneu_bad
!  integer                              :: num_args, ix
  integer                              :: ix
!  character(len=12), dimension(:), allocatable :: args
  character(len=32)                    :: arg

!----------------------------------------------------------------------------
  inlo = 1       ! specify LO only[0] or complete NLO (slower)[1]           !
!                ! results: LO     - LO, degenerate squarks, decoupling on  !
!                !          NLO    - NLO, degenerate squarks, decoupling on !
!                !          LO_ms  - LO, free squark masses, decoupling off !
!                !          NLO_ms - NLO, free squark masses, scaled        !
!                ! all numerical errors (hopefully) better than 1%          !
!                ! follow Vegas iteration on screen to check                !
!----------------------------------------------------------------------------

!----------------------------------------------------------------------------
  isq_ng_in = 1     ! specify degenerate [0] or free [1] squark masses      !
                    ! [0] means Prospino2.0 with average squark masses      !
                    ! [0] invalidates isquark_in switch                     !
!----------------------------------------------------------------------------

!----------------------------------------------------------------------------
  icoll_in = 1      ! collider : tevatron[0], lhc[1]                        !
  energy_in = 13000 ! collider energy in GeV                                !
!----------------------------------------------------------------------------

!----------------------------------------------------------------------------
  i_error_in = 0    ! with central scale [0] or scale variation [1]         !
!----------------------------------------------------------------------------

!----------------------------------------------------------------------------
  final_state_in = 'tb'                                                     !
!                                                                           !
!                   ng     neutralino/chargino + gluino                     !
!                   ns     neutralino/chargino + squark                     !
!                   nn     neutralino/chargino pair combinations            !
!                   ll     slepton pair combinations                        !
!                   sb     squark-antisquark                                !
!                   ss     squark-squark                                    !
!                   tb     stop-antistop                                    !
!                   bb     sbottom-antisbottom                              !
!                   gg     gluino pair                                      !
!                   sg     squark + gluino                                  !
!                   lq     leptoquark pairs (using stop1 mass)              !
!                   le     leptoquark plus lepton (using stop1 mass)        !
!                   hh     charged Higgs pairs (private code only!)         !
!                   ht     charged Higgs with top (private code only!)      !
!                                                                           !
!  squark and antisquark added, but taking into account different sb or ss  !
!----------------------------------------------------------------------------

!----------------------------------------------------------------------------
  ipart1_in = 1                                                             !
  ipart2_in = 1                                                             !
!                                                                           !
!  final_state_in = ng,ns,nn                                                !
!  ipart1_in   = 1,2,3,4  neutralinos                                       !
!                5,6      positive charge charginos                         !
!                7,8      negative charge charginos                         !
!  ipart2_in the same                                                       !
!      chargino+ and chargino- different processes                          !
!                                                                           !
!  final_state_in = ll                                                      !
!  ipart1_in   = 0        sel,sel + ser,ser  (first generation)             !
!                1        sel,sel                                           !
!                2        ser,ser                                           !
!                3        snel,snel                                         !
!                4        sel+,snl                                          !
!                5        sel-,snl                                          !
!                6        stau1,stau1                                       !
!                7        stau2,stau2                                       !
!                8        stau1,stau2                                       !
!                9        sntau,sntau                                       !
!               10        stau1+,sntau                                      !
!               11        stau1-,sntau                                      !
!               12        stau2+,sntau                                      !
!               13        stau2-,sntau                                      !
!               14        H+,H- in Drell-Yan channel                        !
!                                                                           !
!  final_state_in = tb and bb                                               !
!  ipart1_in   = 1        stop1/sbottom1 pairs                              !
!                2        stop2/sbottom2 pairs                              !
!                                                                           !
!  note: otherwise ipart1_in,ipart2_in have to set to one if not used       !
!                                                                           !
!----------------------------------------------------------------------------

!----------------------------------------------------------------------------
  isquark1_in = 0                                                           !
  isquark2_in = 0                                                           !
!                                                                           !
!  for LO with light-squark flavor in the final state                       !
!  isquark1_in     =  -5,-4,-3,-2,-1,+1,+2,+3,+4,+5                         !
!                    (bL cL sL dL uL uR dR sR cR bR) in CteQ ordering       !
!  isquark1_in     = 0 sum over light-flavor squarks throughout             !
!                      (the squark mass in the data files is then averaged) !
!                                                                           !
!  flavors in initial state: only light-flavor partons, no bottoms          !
!                            bottom partons only for Higgs channels         !
!                                                                           !
!  flavors in final state: light-flavor quarks summed over five flavors     !
!                                                                           !
!----------------------------------------------------------------------------
  
  call PROSPINO_OPEN_CLOSE(0)                                                            ! open all input/output files


!    Check that 2*mgluino < sqrts and that mneu/mchi masses < mz:
   ix = 0
   mneu_bad = "mnebad"
   mgo_bad = "mgobad"
   do_mneu = .TRUE.
   do_mgo = .TRUE.

   do
       call get_command_argument(ix, arg)
       if (len_trim(arg) == 0) exit
       if (arg == mneu_bad) then
           do_mneu = .FALSE.
       endif
       if (arg == mgo_bad) then
           do_mgo = .FALSE.
       endif
!       write(*,* trim(arg))
       ix = ix + 1
       ! now parse the argument as you wish
   end do
! FROM hadronsgg.f: IF (ENERGY.LT.2*MG) THEN
  if (.NOT. do_mgo) PRINT *,'ENERGY TOO SMALL TO PRODUCE GLUINO-GLUINO'
!  if ( (abs(m1)+abs(m2)) < mz ) then
  if (.NOT. do_mneu) PRINT *," IFCT_NN_X12: masses low, Z decays might be more suitable "


!  final_state_in = 'ng'
!  do ipart1_in = 1,8,1
!     print*, " computing process final_state_in, ipart1, ipart2",final_state_in,ipart1_in,ipart2_in
!     call PROSPINO(inlo,isq_ng_in,icoll_in,energy_in,i_error_in,final_state_in,ipart1_in,ipart2_in,isquark1_in,isquark2_in) ! actual prospino call
!  end do

!  final_state_in = 'ns'
!!   Note that NLO cross sections are not available for this process, so LO will
!!   be taken instead (through first argument given to prospino that is 0).
!  do ipart1_in = 1,8,1
!     print*, " computing process final_state_in, ipart1, ipart2",final_state_in,ipart1_in,ipart2_in
!     call PROSPINO(0,isq_ng_in,icoll_in,energy_in,i_error_in,final_state_in,ipart1_in,ipart2_in,isquark1_in,isquark2_in) ! actual prospino call
!  end do

!   Skip this if nn masses < Z mass
!   (to be indicated with command line argument):
  if (do_mneu) then
      final_state_in = 'nn'
      do ipart1_in = 1,8,1
         do ipart2_in = 1,ipart1_in,1
            call PROSPINO(inlo,isq_ng_in,icoll_in,energy_in,i_error_in,final_state_in,ipart1_in,ipart2_in,isquark1_in,isquark2_in) ! actual prospino call
         end do
      end do
  endif

!  final_state_in = 'll'
!  do ipart1_in = 0,14,1
!     ipart2_in = 1
!     call PROSPINO(inlo,isq_ng_in,icoll_in,energy_in,i_error_in,final_state_in,ipart1_in,ipart2_in,isquark1_in,isquark2_in) ! actual prospino call
!  end do


!   Skip this if gluino mass too large
!   (to be indicated with command line argument):
  if (do_mgo) then
      final_state_in = 'gg'
      ipart1_in = 1
      call PROSPINO(inlo,isq_ng_in,icoll_in,energy_in,i_error_in,final_state_in,ipart1_in,ipart2_in,isquark1_in,isquark2_in) ! actual prospino call
  endif

!  final_state_in = 'sb'
!  ipart1_in = 1
!  ipart2_in = 1
!  call PROSPINO(inlo,isq_ng_in,icoll_in,energy_in,i_error_in,final_state_in,ipart1_in,ipart2_in,isquark1_in,isquark2_in) ! actual prospino call
!
!  final_state_in = 'sg'
!  ipart1_in = 1
!  ipart2_in = 1
!  call PROSPINO(inlo,isq_ng_in,icoll_in,energy_in,i_error_in,final_state_in,ipart1_in,ipart2_in,isquark1_in,isquark2_in) ! actual prospino call
!
!  final_state_in = 'ss'
!  ipart1_in = 1
!  ipart2_in = 1
!  call PROSPINO(inlo,isq_ng_in,icoll_in,energy_in,i_error_in,final_state_in,ipart1_in,ipart2_in,isquark1_in,isquark2_in) ! actual prospino call
!
!  final_state_in = 'bb'
!  do ipart1_in = 1,2,1
!     ipart2_in = ipart1_in
!     call PROSPINO(inlo,isq_ng_in,icoll_in,energy_in,i_error_in,final_state_in,ipart1_in,ipart2_in,isquark1_in,isquark2_in) ! actual prospino call
!  end do
!
!  final_state_in = 'tb'
!  do ipart1_in = 1,2,1
!     ipart2_in = ipart1_in
!     call PROSPINO(inlo,isq_ng_in,icoll_in,energy_in,i_error_in,final_state_in,ipart1_in,ipart2_in,isquark1_in,isquark2_in) ! actual prospino call
!  end do


!  call PROSPINO_CHECK_HIGGS(final_state_in)                                              ! lock Higgs final states
!  call PROSPINO_CHECK_FS(final_state_in,ipart1_in,ipart2_in,lfinal)                      ! check final state 
!  if (.not. lfinal ) then
!     print*, " final state not correct ",final_state_in,ipart1_in,ipart2_in
!     call HARD_STOP                                                                      ! finish if final state bad
!  end if
!  call PROSPINO_CHECK_HIGGS(final_state_in)                                              ! lock Higgs final states
!  call PROSPINO_CHECK_FS(final_state_in,ipart1_in,ipart2_in,lfinal)                      ! check final state 
!  if (.not. lfinal ) then
!     print*, " final state not correct ",final_state_in,ipart1_in,ipart2_in
!     call HARD_STOP                                                                      ! finish if final state bad
!  end if

!  call PROSPINO(inlo,isq_ng_in,icoll_in,energy_in,i_error_in,final_state_in,ipart1_in,ipart2_in,isquark1_in,isquark2_in) ! actual prospino call

!----------------------------------------------------------------------------
!  input file: prospino.in.leshouches                                       !
!              use block MASS for masses, plus low-energy mixing matrices   !
!                                                                           !
!  output file: prospino.dat   for compact output format                    !
!               prospino.dat2  for long output including subchannels        !
!               prospino.dat3  lo file for masses, flags, etc               !
!----------------------------------------------------------------------------
  call PROSPINO_OPEN_CLOSE(1)                                                            ! close all input/output files 

end program main


