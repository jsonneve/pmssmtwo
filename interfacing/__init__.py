"""
.. module:: interfacing.__init__
   :synopsis: This package provides interfaces to hep
              programs so that they are easier to use.
              Examples:
                  * checkmate
                  * nllfast
                  * spheno
                  * delphes
                  * smodels
                  * ...

"""

import os

basepath = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..'))
