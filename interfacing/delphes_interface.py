#!/usr/bin/env python3

"""
Interface to Delphes.
"""

from __future__ import division
import os
import shutil as sh
import subprocess
import math
import locale
import glob
import gzip

def run_delphes(hepfile, delphescard, delphesdir, rootfile=None):
    """
    Run delphes on a pythia HEP file.
    Return a ROOT file.
    """
    if hepfile.endswith('gz'):
        hepfile = gunzip(hepfile)
    delphes_stdhep = os.path.join(delphesdir, 'DelphesSTDHEP')
    if rootfile == None:
        rootfile = hepfile[:hepfile.rindex('.')] + '.root'
    out = subprocess.check_output([delphes_stdhep, delphescard, rootfile, hepfile])
    return rootfile, out



def gunzip(zipped_filename):
        """ (str) -> str
        Open a zipped file and unzip it; return the unzipped
        filename.
        Assuming gzipped files with extension .gz
        """
        if zipped_filename[-2:] != 'gz':
            return zipped_filename
        zipped_file = gzip.open(zipped_filename, "rb")
        unzipped_filename = zipped_filename[:-3]
        unzipped_file = open(unzipped_filename, 'w')
        unzipped_file.writelines(zipped_file)
        zipped_file.close()
        unzipped_file.close()
        return unzipped_filename


