#!/usr/bin/env python3

"""
For interfacing with the program SPheno.
See spheno.hepforge.org.
"""

import sys
import os
import subprocess
import shutil as sh
sys.path.append('..')
from tools import os_tools

#from tools import os_tools

def spheno_card(spheno_dir, template, output_file, mzero, mhalf, azero = 0,
        tanbeta = 30, signmu = +1, verbose=False):
        """ (str, str, str, int, int, float, float, int) -> str
        Modifies the CMSSM parameters in the les houches spheno template
        and saves it as LesHouches.in for spheno in the sphenodir.
        Then it generates a card in spheno, saves it as SPheno.spc.
        , and returns the output_file name. The file is also saved to
        output_file.
        >>> testoutput = spheno_card(os.path.join(os_tools.home_dir(), 'bin/spheno/'), os.path.join(os_tools.home_dir(), 'src/templates/LesHouches.in.mSUGRA_template'), 'test_spheno_slha.out', 100, 100)
        >>> testoutput
        'test_spheno_slha.out'
        >>> import os
        >>> os.path.exists(testoutput)
        True
        >>> sphenonew = open(testoutput, 'r')
        >>> lines = sphenonew.readlines()
        >>> 'mzero' in lines
        False
        """
        params = {'mzero': mzero, 'mhalf': mhalf, 'azero': azero, 'tanbeta':
                tanbeta, 'signmu': signmu}
        return slha(spheno_dir, template, output_file, params, verbose)


def slha(spheno_dir, template, output_file, params, verbose=False):
        """ (str, str, str, int, int, float, float, int) -> str
        Modifies the parameters in the given les houches spheno template
        and saves it as LesHouches.in for spheno in the sphenodir.
        Then it generates a card in spheno, saves it as SPheno.spc.
        , and returns the output_file name. The file is also saved to
        output_file.
        """

        # Write the parameters to the file to be used in SPheno:
        leshouches_template = open(template, "r")
        leshouches_filename = os.path.join(spheno_dir, "LesHouches.in")
        leshouches_file = open(leshouches_filename, "w")
        lines = leshouches_template.readlines()
        for line in lines:
            for param in params:
                line = line.replace(param, str(params[param]))
                #line = line.replace("mzero", str(mzero))
                #line = line.replace("mhalf", str(mhalf))
                #line = line.replace("azero", str(azero))
                #line = line.replace("tanbeta", str(tanbeta))
                #line = line.replace("signmu", str(signmu))
            leshouches_file.write(line)
        leshouches_template.close()
        leshouches_file.close()
        if verbose:
            print("Will use Les Houches file:", leshouches_filename)

        # Save current dir:
        current_dir = os.getcwd()

        # Go to spheno dir:
        os.chdir(spheno_dir)

        # Run SPheno
        if verbose:
            print("Will now run spheno in ", os.getcwd())
            print("Running make...")
        out = subprocess.check_output(["make"])
        if verbose:
            print("Running SPheno...")
        out = subprocess.check_output(["./bin/SPheno"])
        if 'problem' in out:
            os.chdir(current_dir)
            return None
        spheno_output = 'SPheno.spc'
        os.chdir(current_dir)
        sh.copyfile(os.path.join(spheno_dir, spheno_output), output_file)
        return output_file



if __name__ == '__main__':
    import doctest
    doctest.testmod()