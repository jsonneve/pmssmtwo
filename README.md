#### Set your path ##
export PYTHONPATH=$PYTHONPATH:/your/path/to/this/repo/

#### Interfacing: install programs when necessary
## For example, for nllfast:
## NLLFast computes just gluino-squark cross sections (no EW) at NLL.
## Get nllfast:
wget 'http://pauli.uni-muenster.de/~cbors_01/nllfast/nllfast-3.1-13TeV.tar.bz2'

## Open the directory and compile the code:
gfortran nllfast-3.1-13TeV.f -o nllfast

## Set the paths in interfacing/nllfast_interface.py.
## Then run 
python nllfast_interface.py


#### To compute cross sections, make a file with slha names:
 % cat slhalist.txt
pMSSM12_MCMC1_27_969542.slha
pMSSM12_MCMC1_28_737434.slha
pMSSM12_MCMC1_8_373637.slha

#### Submit on e.g. cms connect in the stash directory using
cross_sections.submit
#### This calls:
compute_xsecs.sh

#### To get nice output from the resulting files, e.g. on lpc:
python write_benchmarks_nicely.py


#### For more information about cms connect:
#### https://ci-connect.atlassian.net/wiki/display/CMS/CMSSW+Analysis+example
#### https://indico.cern.ch/event/533066/contributions/2210983/attachments/1293988/1928544/CMSConnect.pdf
#### https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookCMSConnect
#### https://connect.uscms.org/signup
#### https://indico.cern.ch/event/533066//

#### For information about CRAB:
#### https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideCMSDataAnalysisSchoolPreExerciseThirdSet

