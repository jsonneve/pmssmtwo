import math
import numpy as np
import integral
import upper_limits

def cm_likelihood(b0, sigmab, nobs, s0, sigmas, randomseed = 0):
    """ Approximative likelihood. AUTHOR: Jamie Tattersall and Sebastian Belkner """
    """ altered by Jory + Wolfgang so it really returns a likelihood instead of a chi2 """
    if randomseed != 0:
      np.random.seed(randomseed)
    n_BkgSig = b0 + s0
    err_BkgSig = math.sqrt(sigmab*sigmab + sigmas*sigmas)
    # Use Gaussian approximation if predicted number of background events is greater than 30 or b+s > 50
    if(b0 > 20):
        T_obs = (pow(nobs - n_BkgSig, 2))/(nobs + (err_BkgSig*err_BkgSig))
        return (1/math.sqrt(2*math.pi*(err_BkgSig**2 + nobs)))*math.exp(-0.5*(pow(nobs - n_BkgSig, 2))/(nobs + (err_BkgSig*err_BkgSig)))
    elif(n_BkgSig > 35):
        T_obs = (pow(nobs - n_BkgSig, 2))/(nobs + (err_BkgSig*err_BkgSig))
        return (1/math.sqrt(2*math.pi*(err_BkgSig**2 + nobs)))*math.exp(-0.5*(pow(nobs - n_BkgSig, 2))/(nobs + (err_BkgSig*err_BkgSig)))
    # For low statistics use gaussian convoluted with Poisson
    else:
        # likelihood distribution
        distSig = 0
        dist = 0
        nb = 0
        nsb = 0
        ntot = 1000000
        for i in range(0,ntot):
            # maximize and throw toys at same
            # draw from poisson
            distSmearBkgSig= np.random.normal(n_BkgSig,err_BkgSig,1)[0] # smearing done by the err_BkgSig
            distSmearNobs = np.random.normal(nobs,err_BkgSig,1)[0] # smearing done by the err_BkgSig
            distSig = np.random.poisson(max(0.000001,distSmearBkgSig))
            distNobs = np.random.poisson(max(0.000001,distSmearNobs)) # Global Maximum Likelihood, Lambda = Nobs
            # how often you draw correctly is your probability
            if distNobs==nobs:
                nb=nb+1
            if distSig==nobs:
                nsb=nsb+1
        if (nb == 0): return None
        LHratio=float(nsb)/float(nb)    # Likelihood ratio
        print(nsb)
        print(nsb/nobs)
        return nsb/(1.0*ntot)
        T_obs = -2.*log(LHratio)          # Value of the observed Teststatistic
        if T_obs < 0.:                   #In case random MC gives negative chi2, set=0
          T_obs = 0.
    return(T_obs)


def sm_likelihood ( nsig, nobs, nb, deltab, deltas, ntoys=100000):
        """ (float, float, float, float, float, int) -> float
        :param nsig: predicted signal
        :param nobs: number of observed events
        :param nb: predicted background
        :param deltab: uncertainty on background
        :param deltas: uncertainty on signal
        :param ntoys: number of toys to use in integral


        See also http://smodels.hephy.at/wiki/llhdeff
        for validation.

        Return the likelihood to observe nobs events given the
        predicted background nb, error on this background deltab,
        expected number of signal events nsig, and if given
        the error on the signal deltas.
        Assume an error of 20% on the signal if no error is given.

        Why not a simple gamma function for the factorial:
        -----------------------------------------------------
        The scipy.stats.poisson.pmf probability mass function
        for the Poisson distribution only works for discrete
        numbers. The gamma distribution is used to create a
        continuous Poisson distribution.

        Why not a simple gamma function for the factorial:
        -----------------------------------------------------
        The gamma function does not yield results for integers
        larger than 170. Since the expression for the Poisson
        probability mass function as a whole should not be huge,
        the exponent of the log of this expression is calculated
        instead to avoid using large numbers.

        # Example of T1 result for mgluino=500, mlsp=200:
        >>> round(sm_likelihood( 384.899, 298, 111.0, 11.0, 0.2*384.899), 4)
        0.0002

        # Example of T1 result for mgluino=700, mlsp=100:
        >>> round(sm_likelihood( 157.57, 2167., 2120.0, 110.0, 0.2*157.57), 3)
        0.002

        # Example of T1 result for mgluino=1100, mlsp=900:
        >>> round(sm_likelihood( 4.82, 2166, 2120.0, 110.0, 0.2*4.82), 3)
        0.003
        """

        total_integrand = 0.

        ## perform double Gaussian integral by Monte Carlo


        # Make sure mean below is not always negative, but limit
        # number of tries to search for lambda_b>0, lambda_s>0
        # to avoid getting stuck:
        max_tries = 50


        for i in range(ntoys):

                # Compute two random numbers:
                smear_b, smear_s = np.random.normal(), np.random.normal()

                # Smear background and signal with a Gaussian
                # using their widths as errors:
                lambda_b = nb + smear_b*deltab
                lambda_s = nsig + smear_s*deltas
                mean = lambda_b + lambda_s
                # smearing is done with random.normal numbers:
                # stats.normal.pdf can also be used but is much slower

                # Make sure these numbers are not always negative:
                # count number of tries to obtain positive mean
                # and stay below a maximum to avoid lengthy
                # computations:
                try_positive = 0

                # Keep searching for positive lambda_b, lambda_s
                # for a maximum number of tries positive_tries:
                while mean < 0. and try_positive < max_tries:
                    try_positive += 1
                    smear_b, smear_s = np.random.normal(), np.random.normal()
                    lambda_b = nb + smear_b*deltab
                    lambda_s = nsig + smear_s*deltas
                    # total predicted
                    mean = lambda_b + lambda_s

                #Cut integral at negative mean values
                if mean <= 0.:
                    continue

                # value of integrand, poisson likelihood value
                # poisson_integrand = exp(-mean) * mean^nobs / nobs! 
                poisson_integrand = np.exp(    nobs * np.log ( mean ) - mean - math.lgamma(nobs + 1 ) )
                total_integrand += poisson_integrand

        # Return the likelihood normalized by the number of toys.
        return total_integrand/float(ntoys)


def integral_likelihood(nsig, nobs, nb, deltab, deltas=None):
    """
    Compute likelihood as poisson*gauss using
    scipy.integrate.
    """
    return integral.likelihood(nsig, nobs, nb, deltab, deltas)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
    d = {'mgluino':  700, 'mlsp':  600, 'nsig':   5.18888, 'nobs':  24.3271,
            'nb': 37.0, 'deltab': 6.0  , 'llhd': 0.00536789 , 'chi2': None}
    nobs = d['nobs']
    nb = d['nb']
    nsig = d['nsig']
    deltab = d['deltab']
    deltas = 0.2*nsig
    print("nobs, nb, deltab, nsig:", nobs, nb, deltab, nsig, deltas)


    print("Checkmate likelihood")
    print("llhd=", cm_likelihood(nb, deltab, nobs, nsig, deltas))
    print("llhdmax=", cm_likelihood(nb, deltab, nobs, nobs - nb,
        math.sqrt(nobs + deltab**2)))

    print("SModelS likelihood")
    print("llhd=", sm_likelihood(nsig, nobs, nb, deltab, deltas))
    print("llhdmax=", sm_likelihood(nobs-deltab, nobs, nb, deltab,
        deltas))

    print("Integral likelihood")
    print("llhd=", integral_likelihood(nsig, nobs, nb, deltab, deltas))
    print("llhdmax=", integral_likelihood(nobs-deltab, nobs, nb, deltab,
        deltas))


