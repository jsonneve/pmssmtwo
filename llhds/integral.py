#!/usr/bin/env python

"""
.. module:: llhd
   :synopsis: Obtain likelihood distribution functions from known variables.
              Using the observed number of events (data) and estimated number
              of background events (total background) with error, estimate the
              likelihood for a given theory prediction of a number of events.
              source: arXiv:1202.3415

.. moduleauthor:: Jory Sonneveld <jory@opmijnfiets.nl>

"""

from __future__ import division # so that 1/2 = 0.5, not 0
from scipy import stats
from scipy import integrate
import math
import numpy as np



def _log_poisson(k, lam):
    """
    Return value for Poisson distribution for integer as well
    as noninteger values.

    Parameters:
    :float k: index of Poisson distribution
    :float lam: positive real number; expected value

    Returns:
    :float poiss: value for continuous Poisson distribution
    at k, lam

    Why not scipy.stats.poisson.pmf:

    The scipy.stats.poisson.pmf probability mass function
    for the Poisson distribution only works for discrete
    numbers. The gamma distribution is used to create a
    continuous Poisson distribution.

    Why not a simple gamma function for the factorial:

    The gamma function does not yield results for integers
    larger than 170. Since the expression for the Poisson
    probability mass function as a whole should not be huge,
    the exponenet of the log of this expression is calculated
    instead to avoid using large numbers.

    Why the check for k > 0:

    In principle k should be a positive number. In the expression
    for the likelihood, this is indeed the case, as k is either
    1) the number of signal+background events, which should be 
    positive;
    2) the number of background events, which in the integral is
    equal to n_observed - s, with s integrated over from 0 to
    n_observed.
    However, to be safe and avoid errors, zero is returned for
    negative k.

    Similarly, zero is returned for zero or negative expected
    background or signal plus background.

    Examples:

    The _log_poisson function should return the same values as the
    scipy.stats.poisson.pmf Poisson probability mass function (pmf):
    >>> round(_log_poisson(4, 5), 12)
    0.175467369768
    >>> round(stats.poisson.pmf(4, 5), 12)
    0.175467369768

    The real Poisson pmf is zero for noninteger values, but the
    _log_poisson function is continuous:
    >>> round(_log_poisson(15.5, 15.5), 11)
    0.10078818483
    >>> stats.poisson.pmf(15.5, 15.5)
    0.0
    
    Check boundaries:
    >>> _log_poisson(-1, 4)
    0
    >>> _log_poisson(3, 0)
    0

    The nonlogarithmic continuous Poisson pmf does not work for values
    of k beyond 170 since factorials and the gamma function do not take
    too large values (they become huge).
    >>> math.exp(-200) * 200**171/math.gamma(171)
    Traceback (most recent call last):
        ...
    OverflowError: long int too large to convert to float

    The regular SciPy Poisson pmf and the _log_poisson pmf can handle
    large values, however:
    >>> round(_log_poisson(171, 200), 11)
    0.00333775744
    >>> round(stats.poisson.pmf(171, 200), 11)
    0.00333775744


    """
    if k < 0 or lam <= 0:
        return 0
    return math.exp(-lam + k*math.log(lam) - math.lgamma(k+1))

def _poisson_double_gauss(var_b, var_s, n_obs, lambda_b, delta_b, lambda_s,
        delta_s=None):
    """
    Using the probability density function fo a poisson convoluted with a
    gaussian, give the likelihood density function.
    For this purpose we use
    poisson(lambda_s + var_b, nobs) * normal(var_b, loc=lambda_b, scale=delta_b)
    It is basically eq. 6.36 in
    http://phys.onmybike.nl/sonneveld_thesis_RWTHAachen.pdf
    with s + b = n_obs and convoluted with a gaussian smearing for the background.

    Parameters:
    :float var_b: the variable background to be integrated over in this llhd
    :int n_obs: known observed number of events (data)
    :float lambda_s: expected number of signal events
    :float n_b: known estimated number of background events
    :float deltab: known error on estimated number of background events
    """
    if delta_s == None: delta_s = 0.2*lambda_s
    return _log_poisson(var_s + var_b, n_obs)*stats.norm.pdf(var_b,\
            loc=lambda_b, scale=math.sqrt(delta_b**2))*stats.norm.pdf(var_s,\
            loc=lambda_s, scale=math.sqrt(delta_s**2))


def _poisson_gauss(var_b, n_obs, lambda_b, delta_b, lambda_s, delta_s=0.0):
    """
    Using the probability density function fo a poisson convoluted with a
    gaussian, give the likelihood density function.
    For this purpose we use
    poisson(lambda_s + var_b, nobs) * normal(var_b, loc=lambda_b, scale=delta_b)
    It is basically eq. 6.36 in
    http://phys.onmybike.nl/sonneveld_thesis_RWTHAachen.pdf
    with s + b = n_obs and convoluted with a gaussian smearing for the background.

    Parameters:
    :float var_b: the variable background to be integrated over in this llhd
    :int n_obs: known observed number of events (data)
    :float lambda_s: expected number of signal events
    :float n_b: known estimated number of background events
    :float deltab: known error on estimated number of background events
    """
    #return _log_poisson(lambda_s + var_b, n_obs) * stats.norm.pdf(var_b, \
    #        loc=lambda_b, scale=math.sqrt(delta_b**2 + delta_s**2))
    return _log_poisson(var_b, n_obs) * stats.norm.pdf(var_b, \
            loc=lambda_b + lambda_s, scale=math.sqrt(delta_b**2 + delta_s**2))


def likelihood(nsig, nobs, nb, deltab, deltas=None, double_gauss=False):
    """
    The likelihood L(lambda_s).

    Parameters:
    :float lambda_s: estimated number of signal events
    :float lambda_b: estimated number of background events
    :int n_obs: observed number of events (data)
    :float deltab: error on estimated number of background events
    :float deltas: error on estimated number of signal events

    Given data from experiment (n_obs, lambda_b, delta_b) and the theory
    prediction for the number of signal events (lambda_s), calculate
    the likelihood for this theory.

    Why two integrals:
    Since the quadpack numeric integration works by increasing
    resolution until the answer no longer improves, the integral is
    zero for most of the interval from zero to infinity, quad may
    wrongly decide that the integral must be close to zero.
    Therefore, the integral is subdivided into two intervals,
    the first of which contains the peak around n_obs, the second
    of which is zero.


    Examples:
    >>> likelihood(20, 80, 60, 1) # mathematica: 0.0442807
    0.0442806517068595
    >>> likelihood(2, 20, 18, 1) # mathematica: 0.0826721
    0.08267210464093687
    >>> likelihood(2, 10, 8, 1) # mathematica: 0.114454
    0.11445412715690656
    >>> likelihood(20, 400, 380, 10) # mathematica: 0.0155288
    0.01783648327386223
    >>> likelihood(20, 200, 180, 10) # mathematica: 0.0230173
    0.02301730107032973
    >>> likelihood(20, 100, 80, 10) # mathematica: 0.0281625
    0.028162503148917616

    Explicit example with real data:
    bin0 = 3-5jets, H_T 500-800 GeV, MHT 200-300 GeV.
    >>> nobs = 6159 # bin0 of 8TeV MHT analysis CMS-SUS-13-012
    >>> nb = 6090 # bin0 of MHT analysis
    >>> deltab = 670 # bin0 of MHT analysis
    >>> ae = 0.0428606 # bin0 efficiencies of MHT analysis msq=600, mlsp=200
    >>> xsec =  0.024330600454 # pb: xsec for sqLsqLbar MG5 LO mgo=1e5, msq=600
    >>> lumi = 19.5 * 1000.0 # pb: luminosity MHT analysis
    >>> lambdas = ae * xsec * lumi
    >>> round(likelihood(lambdas, nobs, nb, deltab), 5) # mathematica: 0.000591327
    0.00059

    # Same example using dummy lambda_s and data from second bin,
    bin1 with MHT=300-450 GeV):
    >>> likelihood(30, 2305, 2280, 270) # mathematica: 0.00145458
    0.0014545780865066913

    """
    #s = integrate.quad(_poisson_gauss, 0, np.infty, args = (nobs, nb,
    #    deltab, nsig, deltas))[0]
    #if s < 1e-4:
        #return s[0]
    if deltas == None:
        deltas = 0.2*nsig
    delta = math.sqrt(deltab**2 + deltas**2)

    a = max(0, nobs - 5*delta)
    b = nobs + 5*delta
    s = integrate.quad(_poisson_gauss, a, b, args = (nobs, nb, deltab,
           nsig, deltas))[0]
    err = 1.
    while err > 0.01:
        old_like = s
        a = max(0, a - 5*delta)
        b = b + 5*delta
        s = integrate.quad(_poisson_gauss, a, b, args = (nobs, nb, deltab,
            nsig, deltas))[0]
        err = abs(old_like - s)/s
    return s

    if double_gauss:
        s = integrate.dblquad(_poisson_double_gauss, 0, 2*nobs, lambda x: 0,
                lambda x: 2*nobs, args=(nobs, nb, deltab, nsig, deltas))[0]
        s += integrate.dblquad(_poisson_double_gauss, 2*nobs, np.infty,
                lambda x: 2*nobs, lambda x: np.infty, args=(nobs, nb, deltab,
                    nsig, deltas))[0]
    else:
        s = integrate.quad(_poisson_gauss, 0, nobs+nb, args = (nobs, nb, deltab,
            nsig, deltas))[0]
        s += integrate.quad(_poisson_gauss, nobs+nb, np.infty, args = (nobs, nb,
            deltab, nsig, deltas))[0]
    return s


if __name__ == '__main__':
    import doctest
    doctest.testmod()