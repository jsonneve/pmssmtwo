import math
import numpy as np
import likelihood
import checkmate_llhds as cm_llhd

def sm_chi2 ( nsig, nobs, nb, deltab, deltas, ntoys=100000):
        """ (float, float, float, float, float, int) -> float
        :param nsig: predicted signal
        :param nobs: number of observed events
        :param nb: predicted background
        :param deltab: uncertainty on background
        :param deltas: uncertainty on signal
        :param ntoys: number of toys to use in integral


        Computes the chi2 for a given number of observed events nobs
        given the predicted background nb, error on this background deltab,
        expected number of signal events nsig and, if given, the error on signal (deltas).
        If deltas is not given, assume an error of 20% on the signal.

        :param nsig: predicted signal (float)
        :param nobs: number of observed events (float)
        :param nb: predicted background (float)
        :param deltab: uncertainty in background (float)
        :param deltas: uncertainty in signal acceptance (float)
        :param ntoys: number of toys to use in integral (int)

        # Example of T1 result for mgluino=500, mlsp=200:
        >>> int(round(sm_chi2( 384.9, 298., 111.0, 11.0, 0.2*384.899)))
        7

        # Example of T1 result for mgluino=700, mlsp=100:
        >>> int(round(sm_chi2( 157.57, 2167., 2120.0, 110.0, 0.2*157.57)))
        1

        # Example of T1 result for mgluino=1100, mlsp=900:
        >>> round(sm_chi2( 4.82, 2166, 2120.0, 110.0, 0.2*4.82), 1)
        0.1


        """
        #Set signal error to 20%, if not defined
        if deltas is None:
            deltas = 0.2*nsig

        # Check that the background is not already excluded by the number
        # of observed events:
        #if nobs - nb < -10:
            # In this case, we do not want to punish the signal also:
        #    return np.nan

        # Compute the likelhood for the null hypothesis (signal hypothesis) H0:
        llhd = likelihood.sm_likelihood ( nsig, nobs, nb, deltab, deltas, ntoys)

        # Compute the maximum likelihood H1, which sits at nsig=nobs-nb:
        maxllhd = likelihood.sm_likelihood ( nobs - nb, nobs, nb, deltab,
                # keep same % error on signal
                deltas_pct * (nobs - nb), ntoys)

        if llhd == 0.:
            return float('inf')
        # Return the test statistic -2log(H0/H1)
        return -2*np.log(llhd/maxllhd)

def integral_chi2(nsig, nobs, nb, deltab, deltas=None):
    """
    Compute chi2 using scipy.integrate.
    """
    llhd = likelihood.integral_likelihood(nsig, nobs, nb, deltab, deltas)
    llhdmax = likelihood.integral_likelihood(nobs - nb, nobs, nb, deltab,
        deltas=math.sqrt(nobs + deltab**2))
    return -2*np.log(llhd/llhdmax)

def cm_chi2(b0, sigmab, nobs, s0, sigmas, randomseed = 0):
    """
    Compute chi2 from checkmate maximizing own altered likelihood.
    """

    llhd = likelihood.cm_likelihood(b0, sigmab, nobs, s0, sigmas, randomseed)
    maxllhd = likelihood.cm_likelihood(b0, sigmab, nobs, nobs- b0, math.sqrt(nobs +
        sigmab**2), randomseed)
    return -2*np.log(llhd/maxllhd)


def cm_test_statistic(b0, sigmab, nobs, s0, sigmas, randomseed = 0):
    """
    Compute chi2 as defined by test statistic in checkmate code.
    """
    return cm_llhd.likelihood(b0, sigmab, nobs, s0, sigmas, randomseed)

def cm_test_statistic_root(b0, sigmab, nobs, s0, sigmas):
    """
    Compute chi2 as defined by test statistic in checkmate code using ROOT.
    """
    cmllhdroot = cm_llhd.likelihoodrootmethod(1.0, nobs, b0, s0,
                    sigmab, sigmas,  [0.0,0.0])


if __name__ == "__main__":
    import doctest
    doctest.testmod()
    d = {'mgluino':  700, 'mlsp':  600, 'nsig':   5.18888, 'nobs':  24.3271,
            'nb': 37.0, 'deltab': 6.0  , 'llhd': 0.00536789 , 'chi2': None}
    nobs = d['nobs']
    nb = d['nb']
    nsig = d['nsig']
    deltab = d['deltab']
    deltas = 0.2*nsig
    print("nobs, nb, deltab, nsig:", nobs, nb, deltab, nsig, deltas)


    print("Checkmate chi2")
    print("chi2=", cm_chi2(nb, deltab, nobs, nsig, deltas))
    print("Checkmate test statistic")
    print("chi2=", cm_test_statistic(nb, deltab, nobs, nsig, deltas))
    print("chi2(ROOT)=", cm_test_statistic_root(nb, deltab, nobs, nsig, deltas))

    print("SModelS chi2")
    print("chi2=", sm_chi2(nsig, nobs, nb, deltab, deltas))


    print("cm chi2 example 1")
    print("cm_chi2( s0=384.9, nobs=298., b0=111.0, sigmab=11.0, sigmas=0.2*384.899)")
    print(cm_chi2( s0=384.9, nobs=298., b0=111.0, sigmab=11.0, sigmas=0.2*384.899))
    print("cm test statistic example 1")
    print("cm_test_statistic( s0=384.9, nobs=298., b0=111.0, sigmab=11.0, sigmas=0.2*384.899)")
    print(cm_test_statistic( s0=384.9, nobs=298., b0=111.0, sigmab=11.0, sigmas=0.2*384.899))
    print("cm test statistic_root example 1")
    print("cm_test_statistic_root( s0=384.9, nobs=298., b0=111.0, sigmab=11.0, sigmas=0.2*384.899)")
    print(cm_test_statistic_root( s0=384.9, nobs=298., b0=111.0, sigmab=11.0, sigmas=0.2*384.899))
    print("integral_chi2( s0=384.9, nobs=298., b0=111.0, sigmab=11.0, sigmas=0.2*384.899)")
    print(integral_chi2( 384.9, 298., 111.0, 11.0, 0.2*384.899))

    print("Integral chi2")
    print("chi2=", integral_chi2(nsig, nobs, nb, deltab, deltas))

