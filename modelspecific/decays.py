#!/usr/bin/env python3
from __future__ import division
import math



def decay_widths_susy(mgo, msq, mlsp):
    '''(int, int, int)
    Calculate SUSY decay widths. Return 
    a dictionary.
    Source: 
    '''
    prefactors = {}
    uL = 'width_uL'
    uR = 'width_uR'
    dL = 'width_dL'
    dR = 'width_dR'
    go = 'gluino_width'
    prefactors[uL]=0.00013930
    prefactors[uR]=0.00222873
    prefactors[dL]=0.00013930
    prefactors[dR]=0.00055719
    total = sum([prefactors[k] for k in prefactors])
    #prefactors[go]=0.078119822
    prefactors[go] = 0.795 / math.log(mgo/0.22)
    widths = {}
    #gluino_width =  prefactors[go] * (mgo**2-msq**2)**2/float(mgo**3)
    wgluino =  prefactors[go] * (mgo**2-msq**2)**2/float(mgo**3)
    widths[go] = wgluino
    for particle in prefactors:
        prefactor = prefactors[particle]
        widths[particle] = prefactor * (msq**2 - mlsp**2)**2/float(msq**3)
    return widths

def lambda_xyz(x, y, z):
    '''
    Return lambda function.
    '''
    return x**2 + y**2 + z**2 - 2*x*y - 2*x*z - 2*y*z

def decay_widths_no_stops(mgo, msq, mlsp, mtop=172.5):
    '''
    Calculate stop decay widths.
    Return a dictionary.
    '''

    go = 'width_gluino'
    ul = 'width_uL'
    dl = 'width_dL'
    ur = 'width_uR'
    dr = 'width_dR'
    squarks = [ul, dl, ur, dr]

    widths = {}

    # Gluino -> t tbar lsp
    # approximate narrow width:
    widths[go] = 5.0

    prefactor_squark = 7.78 / math.log(msq/0.22)
    for squark in squarks:
        wsquark =  prefactor_squark * (msq**2-mgo**2)**2/float(msq**3)
        widths[squark] = wsquark

    return widths


def decay_widths_t5tbw(mchi, mstop, mlsp, mtop=172.5):
    '''
    Calculate stop and chargino decay widths.
    Return a dictionary.
    '''

    t1 = 'width_stop'
    x1 = 'width_chargino'

    widths = {}

    # Stop -> top + LSP
    stops = {t1: mstop}
    widths[t1] = 1.0 # use narrow width approx for now

    # Chargino -> W + LSP
    widths[x1] = 1.0 # use narrow width approx

    return widths


def decay_widths_stops(mgo, msq, mstop1, mlsp, mtop=172.5):
    '''
    Calculate stop decay widths.
    Return a dictionary.
    '''

    t1 = 'width_stop'
    go = 'width_gluino'
    ul = 'width_uL'
    dl = 'width_dL'
    ur = 'width_uR'
    dr = 'width_dR'
    squarks = [ul, dl, ur, dr]

    widths = {}

    # Stop -> top + LSP
    stops = {t1: mstop1}
    widths[t1] = 1.0 # use narrow width approx for now
    #for t in stops:
    #    mstop = stops[t]
    #    prefactor_sta = 1.0
    #    prefactor_stb = 1.0
    #    lambdaxyz = lambda_xyz(mstop**2, mtop**2, mlsp**2)

    #    # Use 0 width if decay is not allowed:
    #    if lambdaxyz < 0:
    #        widths[t] = 0
    #        continue

    #    # If allowed, calculate decay:
    #    sqrt_lambda = math.sqrt(lambdaxyz)
    #    widths[t] = prefactor_sta * (mstop**2 - mtop**2 - mlsp**2)
    #    widths[t] -= prefactor_stb * mtop * mlsp
    #    widths[t] *= sqrt_lambda/mstop**3

    # Gluino -> stop + top
    widths[go] = 5. # approximate narrow-width for now
    #prefactor_go1 = 1.0
    #prefactor_go2 = 1.0
    #lambdaxyz = lambda_xyz(mgo**2, mstop1**2, mtop**2)
    #if lambdaxyz < 0:
    #    widths[go] = 0
    #else:
    #    sqrt_lambda = math.sqrt(lambdaxyz)
    #    widths[go] = prefactor_go1 * (mstop1**2 - mstop1**2 + mtop**2)
    #    widths[go] += prefactor_go2 * mgo * mtop
    #    widths[go] *= sqrt_lambda/mgo**3

    #prefactor_squark = 0.795 / math.log(msq/0.22)
    prefactor_squark = 7.78 / math.log(msq/0.22)
    #prefactor_squark = 0.888
    for squark in squarks:
        wsquark =  prefactor_squark * (msq**2-mgo**2)**2/float(msq**3)
        widths[squark] = wsquark

    return widths


if __name__ == '__main__':
    import doctest
    doctest.testmod()
