"""
.. module:: modelspecific.__init__
   :synopsis: This package provides manipulations done
              specific to a (BSM) model.
              Examples are computing masses or decays.
              Examples:
                  * cMSSM
                  * simplified models
                  * ...

"""

import os

basepath = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..'))
