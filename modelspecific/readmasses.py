import glob
#import sql3 as sql
import os

import socket

# Define homedir
name = socket.gethostname()
if 'top' in name or 'streep' in name or 'koekblik' in name:
    HOME = '/home/jory/rwth/'
else: 
    HOME = '/net/home/lxtsfs1/tpe/sonneveld/'

def slha_files(path):
    """
    Return all slhafiles in the path.
    """
    return glob.glob(os.path.join(path, '*'))


def cmssm_params(slhafile):
    """
    Extract cMSSM params from slhafile.
    """
    params = {}
    with open(slhafile, 'r') as a:
        l = a.readline()
        for param in ['m0', 'm12', 'tanb', 'cos(phase_mu)', 'A0']:
            while l != '' and not '# ' + param in l:
                l = a.readline()
            params[param] = float(l.split()[1])

    return params




def read_mass(card, cmssm=False):
    """
    Read masses from slha file.

    """
    masses = ['~b_1', '~t_1', '~g', '~chi_10']
    if cmssm:
        masses = ['m_t', 'm0', 'm12'] + masses
    cardopen = open(card, 'r')
    line = cardopen.readline()
    d = {}

    for mass in masses:
        while line != '' and mass not in line:
            line = cardopen.readline()
        print(line, mass)
        d[mass] = float(line.split()[1])

    d['msq'] = 0
    cardopen.seek(0)
    for quark in ['d', 'u', 's', 'c']:
        for hand in ['L', 'R']:
            mass = '~' + quark + '_' + hand
            while line != '' and mass not in line:
                line = cardopen.readline()
            d['msq'] += float(line.split()[1])

    d['msq'] /= 8.
    # Rename
    rename = {'msq': 'msq'}
    if cmssm:
        rename.update({'m0': 'mzero', 'm12': 'mhalf', 'm_t': 'mtop'})
    rename['~b_1'] = 'msbottom'
    rename['~t_1'] = 'mstop'
    rename['~g'] = 'mgo'
    rename['~chi_10'] = 'mlsp'

    return dict([(rename[k], d[k]) for k in rename])
    # python3
    #return {rename[k]: d[k] for k in rename}


if __name__ == "__main__":
    import doctest
    doctest.testmod()
    #cards = '../cmssm_cards_nll_smod19/*'
    #nll = glob.glob(cards)


    #db = os.path.join(HOME, 'globalfits/tgq/TGQtttt.sql')
    #tb = 'masses'
    #for card in nll:
    #    todb = read_mass(card)
    #    sql.dict_to_db(todb, db, tb)

