#!/usr/bin/env python3

def params_from_pmssm10(pars):
    """
    Return the pmssm parameters for the simplified
    pMSSM10 model using its various assumptions.
    """
    params = {}
    # tanbeta stays tanbeta:
    params['tanbeta'] = pars['tanbeta']

    # m1, m2, m3 stay the same
    params['mone'] = pars['mone']
    params['mtwo'] = pars['mtwo']
    params['mthree'] = pars['mthree']

    # all three a parameters are assumed to be equal:
    params['atop'] = params['abottom'] = params['atau'] = pars['a']

    # the mu parameter stays the same
    params['muparam'] = pars['muparam']

    # ma stays the same
    params['mapolemass'] = pars['mapolemass']

    # The three slepton masses are assumed to be the same:
    params['meleft'] = params['mmuleft'] = params['mtauleft'] = pars['mslepton']
    # The same for the right-slepton masses:
    params['meright'] = params['mmuright'] = params['mtauright'] = pars['mslepton']

    # msq is equal for mul, mdl, msl, mcl:
    params['msqleft1'] = params['msqleft2'] = pars['msquark']
    # The same for the right-squark versions:
    params['msupright'] = params['msdownright'] = pars['msquark']
    params['msstrangeright'] = params['mscharmright'] = pars['msquark']

    # Third generation squarks have one mass:
    params['msqleft3'] = pars['msquark3']
    params['mstopright'] = params['msbottomright'] = pars['msquark3']

    return params


